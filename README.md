# Pokretanje projekta
- Potrebno je skinuti projekat sa linka https://gitlab.com/majastamenic/qacube/-/tree/qacube-gitlab-info-majastamenic

Baza podataka: MySQL
  - Baza se nalazi na portu 3306
  - Potrebno je kreirati bazu pod nazivom gitlab_info
  - Takodje, šifra za bazu je: database, a username: root
  - U sklopu foldera pharmacy/src/main/resources nalazi se skripta import.sql koje se pokreće pokretanjem backend projekta.
   
Backend:
  - U intellij-u otvorite folder Backend
  - Intellij će sam da importuje potrebne fajlove
  - U desnom ivici programa ima toolbar. Otvorite Maven, zatim Lifecycle i pokrenite compile.
  - Backend se nalazi na portu 8080.
  
Frontend:
  - Morate instalirati angular. Uputstvo:  https://cli.angular.io/
  - Otvorite Vaš Visual Studio Code i importujte folder Frontend
  - Terminal pozicionirajte u folder Frontend uz pomoć komand cd naziv_foldera
  - Zatim u terminal unesite komande:
```sh
                                $ npm install
                                $ ng serve
```

- Frontend ce biti pokrenut na portu `4200`. Ukoliko želite da frontend pokrenete na nekom drugom portu potrebno je umesto ng serve, izvršiti komandu ng serve --port željeni_port. Primer: `ng serve --port 8080`
- Takodje frontend možete pokrenuti i uz pomoć komande - `npm start`

### Pozdrav, Maja Stamenić

