(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["application-application-module"],{

/***/ "48N5":
/*!*****************************************************************************!*\
  !*** ./src/app/component/nav-bar/application/application-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: ApplicationRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationRoutingModule", function() { return ApplicationRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _add_application_add_application_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-application/add-application.component */ "prkM");
/* harmony import */ var _application_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./application.component */ "XUBd");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");





const routes = [
    { path: '', component: _application_component__WEBPACK_IMPORTED_MODULE_2__["ApplicationComponent"] },
    { path: 'add', component: _add_application_add_application_component__WEBPACK_IMPORTED_MODULE_1__["AddApplicationComponent"] },
];
class ApplicationRoutingModule {
}
ApplicationRoutingModule.ɵfac = function ApplicationRoutingModule_Factory(t) { return new (t || ApplicationRoutingModule)(); };
ApplicationRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: ApplicationRoutingModule });
ApplicationRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](ApplicationRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "NVD7":
/*!*********************************************************************!*\
  !*** ./src/app/component/nav-bar/application/application.module.ts ***!
  \*********************************************************************/
/*! exports provided: ApplicationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationModule", function() { return ApplicationModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _application_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./application-routing.module */ "48N5");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");



class ApplicationModule {
}
ApplicationModule.ɵfac = function ApplicationModule_Factory(t) { return new (t || ApplicationModule)(); };
ApplicationModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: ApplicationModule });
ApplicationModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _application_routing_module__WEBPACK_IMPORTED_MODULE_1__["ApplicationRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](ApplicationModule, { imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _application_routing_module__WEBPACK_IMPORTED_MODULE_1__["ApplicationRoutingModule"]] }); })();


/***/ })

}]);
//# sourceMappingURL=application-application-module.js.map