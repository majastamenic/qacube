(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["component-nav-bar-navbar-module"],{

/***/ "r28+":
/*!****************************************************!*\
  !*** ./src/app/component/nav-bar/navbar.module.ts ***!
  \****************************************************/
/*! exports provided: NavbarModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarModule", function() { return NavbarModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _navbar_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navbar-routing.module */ "swsR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");



class NavbarModule {
}
NavbarModule.ɵfac = function NavbarModule_Factory(t) { return new (t || NavbarModule)(); };
NavbarModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: NavbarModule });
NavbarModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _navbar_routing_module__WEBPACK_IMPORTED_MODULE_1__["NavbarRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](NavbarModule, { imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _navbar_routing_module__WEBPACK_IMPORTED_MODULE_1__["NavbarRoutingModule"]] }); })();


/***/ }),

/***/ "swsR":
/*!************************************************************!*\
  !*** ./src/app/component/nav-bar/navbar-routing.module.ts ***!
  \************************************************************/
/*! exports provided: NavbarRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarRoutingModule", function() { return NavbarRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _chat_chat_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./chat/chat.component */ "EJ1J");
/* harmony import */ var _commit_commit_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./commit/commit.component */ "qRRk");
/* harmony import */ var _connection_connection_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./connection/connection.component */ "S2BP");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "ataK");
/* harmony import */ var _merge_request_merge_request_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./merge-request/merge-request.component */ "oCnx");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "fXoL");








const routes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'connection', component: _connection_connection_component__WEBPACK_IMPORTED_MODULE_3__["ConnectionComponent"] },
    { path: 'application', loadChildren: () => __webpack_require__.e(/*! import() | application-application-module */ "application-application-module").then(__webpack_require__.bind(null, /*! ./application/application.module */ "NVD7")).then(mod => mod.ApplicationModule) },
    { path: 'merge-request', component: _merge_request_merge_request_component__WEBPACK_IMPORTED_MODULE_5__["MergeRequestComponent"] },
    { path: 'commit', component: _commit_commit_component__WEBPACK_IMPORTED_MODULE_2__["CommitComponent"] },
    { path: 'chat', component: _chat_chat_component__WEBPACK_IMPORTED_MODULE_1__["ChatComponent"] }
];
class NavbarRoutingModule {
}
NavbarRoutingModule.ɵfac = function NavbarRoutingModule_Factory(t) { return new (t || NavbarRoutingModule)(); };
NavbarRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({ type: NavbarRoutingModule });
NavbarRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](NavbarRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ })

}]);
//# sourceMappingURL=component-nav-bar-navbar-module.js.map