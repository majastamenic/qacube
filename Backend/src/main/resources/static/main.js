(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Sixtest01\Desktop\p\qacube\Backend\src\main\Frontend\src\main.ts */"zUnb");


/***/ }),

/***/ "1JXD":
/*!*********************************************************!*\
  !*** ./src/app/component/service/web-socket.service.ts ***!
  \*********************************************************/
/*! exports provided: WebSocketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebSocketService", function() { return WebSocketService; });
/* harmony import */ var _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @stomp/ng2-stompjs */ "MWWs");

class WebSocketService {
    constructor(socketUrl, streamUrl) {
        const stompConfig = {
            url: socketUrl,
            headers: {
                login: '',
                passcode: ''
            },
            heartbeat_in: 0,
            heartbeat_out: 20000,
            reconnect_delay: 5000,
            debug: true
        };
        this.stompService = new _stomp_ng2_stompjs__WEBPACK_IMPORTED_MODULE_0__["StompService"](stompConfig);
        this.messages = this.stompService.subscribe(streamUrl);
    }
    stream() {
        return this.messages;
    }
    send(url, message) {
        return this.stompService.publish(url, JSON.stringify(message));
    }
    state() {
        return this.stompService.state;
    }
}


/***/ }),

/***/ "6x5B":
/*!**************************************************!*\
  !*** ./src/app/component/login/login.service.ts ***!
  \**************************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var _util_paths__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../util/paths */ "9ZzF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angularx-social-login */ "ahC7");




class LoginService {
    constructor(httpClient, authService) {
        this.httpClient = httpClient;
        this.authService = authService;
    }
    login(user) {
        return this.httpClient.post(_util_paths__WEBPACK_IMPORTED_MODULE_0__["USER_PATH"], user);
    }
    isUserLogin() {
        const user = sessionStorage.getItem('user');
        return !(user == null);
    }
    logOut() {
        sessionStorage.removeItem('user');
        this.authService.signOut();
    }
    loginGoogle(email) {
        return this.httpClient.get(_util_paths__WEBPACK_IMPORTED_MODULE_0__["USER_PATH"] + '/' + email);
    }
}
LoginService.ɵfac = function LoginService_Factory(t) { return new (t || LoginService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](angularx_social_login__WEBPACK_IMPORTED_MODULE_3__["SocialAuthService"])); };
LoginService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: LoginService, factory: LoginService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "7Ym6":
/*!********************************************************!*\
  !*** ./src/app/component/nav-bar/chat/chat.service.ts ***!
  \********************************************************/
/*! exports provided: ChatService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatService", function() { return ChatService; });
/* harmony import */ var src_app_util_paths__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/util/paths */ "9ZzF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class ChatService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getAll() {
        return this.httpClient.get(src_app_util_paths__WEBPACK_IMPORTED_MODULE_0__["USER_PATH"]);
    }
    getMyMessages(fromLogin) {
        return this.httpClient.get(src_app_util_paths__WEBPACK_IMPORTED_MODULE_0__["CHAT_PATH"] + '?receiverName=' + fromLogin);
    }
    sendMessage(message) {
        return this.httpClient.post(src_app_util_paths__WEBPACK_IMPORTED_MODULE_0__["BACKEND_PATH"] + '/chat', message);
    }
}
ChatService.ɵfac = function ChatService_Factory(t) { return new (t || ChatService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
ChatService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: ChatService, factory: ChatService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "9ZzF":
/*!*******************************!*\
  !*** ./src/app/util/paths.ts ***!
  \*******************************/
/*! exports provided: BACKEND_PATH, CONNECTION_PATH, APPLICATION_PATH, MERGE_PATH, COMMIT_PATH, USER_PATH, CHAT_PATH, GRAPHQL_PATH */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BACKEND_PATH", function() { return BACKEND_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CONNECTION_PATH", function() { return CONNECTION_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APPLICATION_PATH", function() { return APPLICATION_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MERGE_PATH", function() { return MERGE_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "COMMIT_PATH", function() { return COMMIT_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_PATH", function() { return USER_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHAT_PATH", function() { return CHAT_PATH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GRAPHQL_PATH", function() { return GRAPHQL_PATH; });
const BACKEND_PATH = 'http://localhost:8080/api';
const CONNECTION_PATH = BACKEND_PATH + '/connection';
const APPLICATION_PATH = BACKEND_PATH + '/application';
const MERGE_PATH = BACKEND_PATH + '/merge-request';
const COMMIT_PATH = BACKEND_PATH + '/commit';
const USER_PATH = BACKEND_PATH + '/user';
const CHAT_PATH = BACKEND_PATH + '/mymessages';
const GRAPHQL_PATH = BACKEND_PATH + '/graphql';


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "EJ1J":
/*!**********************************************************!*\
  !*** ./src/app/component/nav-bar/chat/chat.component.ts ***!
  \**********************************************************/
/*! exports provided: ChatComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatComponent", function() { return ChatComponent; });
/* harmony import */ var _chat_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./chat.model */ "fBwR");
/* harmony import */ var _login_login_models__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../login/login.models */ "Hc/g");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _chat_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./chat.service */ "7Ym6");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../login/login.service */ "6x5B");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");









function ChatComponent_li_10_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "button", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ChatComponent_li_10_Template_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r5); const user_r2 = ctx.$implicit; const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r4.selectUser(user_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const user_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](user_r2.email);
} }
function ChatComponent_li_18_p_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const m_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", m_r6.messageContent, " ");
} }
function ChatComponent_li_18_p_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "p", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const m_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", m_r6.messageContent, " ");
} }
function ChatComponent_li_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ChatComponent_li_18_p_1_Template, 2, 1, "p", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, ChatComponent_li_18_p_2_Template, 2, 1, "p", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const m_r6 = ctx.$implicit;
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", m_r6.senderName == ctx_r1.selectedUser.email && m_r6.receiverName == ctx_r1.fromLogin);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", m_r6.senderName == ctx_r1.fromLogin && m_r6.receiverName == ctx_r1.selectedUser.email && ctx_r1.fromLogin != ctx_r1.selectedUser.email);
} }
const WEBSOCKET_URL = 'ws://localhost:8080/socket';
const EXAMPLE_URL = '/topic/server-broadcaster';
class ChatComponent {
    constructor(chatService, loginService, router, toastr) {
        this.chatService = chatService;
        this.loginService = loginService;
        this.router = router;
        this.toastr = toastr;
        this.selectedUser = new _login_login_models__WEBPACK_IMPORTED_MODULE_1__["User"]();
        this.messageDto = new _chat_model__WEBPACK_IMPORTED_MODULE_0__["MessageDto"]();
        this.message = '';
        this.users = [];
        this.messages = [];
    }
    ngOnInit() {
        if (!this.loginService.isUserLogin()) {
            this.router.navigate(['login']);
        }
        else {
            this.fromLogin = sessionStorage.getItem('user');
        }
        this.chatService.getAll().subscribe((response) => {
            for (const u of response) {
                this.users.push(new _login_login_models__WEBPACK_IMPORTED_MODULE_1__["User"]().deserialize(u));
            }
        });
        this.loadMessage();
    }
    sendMessage() {
        this.messageDto.senderName = this.fromLogin;
        this.messageDto.receiverName = this.selectedUser.email;
        this.chatService.sendMessage(this.messageDto).subscribe((response) => {
            this.messageDto = new _chat_model__WEBPACK_IMPORTED_MODULE_0__["MessageDto"]();
        });
    }
    selectUser(user) {
        this.selectedUser = new _login_login_models__WEBPACK_IMPORTED_MODULE_1__["User"]().deserialize(user);
        this.chatService.getMyMessages(this.fromLogin).subscribe((response) => {
            this.messages = response;
        });
    }
    loadMessage() {
        this.chatService.getMyMessages(this.fromLogin).subscribe((response) => {
            for (const message of response) {
                this.messages.push(new _chat_model__WEBPACK_IMPORTED_MODULE_0__["MessageDto"]().deserialize(message));
            }
        });
    }
}
ChatComponent.ɵfac = function ChatComponent_Factory(t) { return new (t || ChatComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_chat_service__WEBPACK_IMPORTED_MODULE_3__["ChatService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_login_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"])); };
ChatComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: ChatComponent, selectors: [["app-chat"]], decls: 23, vars: 4, consts: [[1, "banner"], [1, "container", "clearfix"], [1, "row"], [1, "column"], ["id", "people-list", 1, "people-list"], ["id", "usersList", 1, "list"], [4, "ngFor", "ngForOf"], [1, "chat"], [1, "chat-header", "clearfix"], [1, "chat-history"], [1, "chat-message", "clearfix"], ["id", "message-to-send", "name", "message-to-send", "placeholder", "Type your message", "rows", "3", 3, "ngModel", "ngModelChange"], ["id", "sendBtn", 3, "click"], [1, "btn", "btn-link", 3, "click"], [4, "ngIf"], ["class", "my", 4, "ngIf"], [1, "my"]], template: function ChatComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](5, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, "Users:");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](8, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "ul", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](10, ChatComponent_li_10_Template, 3, 1, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](13, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](14, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](16, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](17, "ul");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](18, ChatComponent_li_18_Template, 3, 2, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](19, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](20, "textarea", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function ChatComponent_Template_textarea_ngModelChange_20_listener($event) { return ctx.messageDto.messageContent = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](21, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ChatComponent_Template_button_click_21_listener() { return ctx.sendMessage(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](22, "Send");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx.users);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](ctx.selectedUser.email);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx.messages);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngModel", ctx.messageDto.messageContent);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"]], styles: [".container[_ngcontent-%COMP%] {\r\n\t\t    margin: 0 auto;\r\n\t\t    width: 750px;\r\n\t\t\theight: 800px;\r\n\t\t    background: whitesmoke;\r\n\t\t    border-radius: 5px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%] {\r\n\t\t    width: 260px;\r\n\t\t    float: left;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%] {\r\n\t\t    padding: 20px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\r\n\t\t    border-radius: 3px;\r\n\t\t    border: none;\r\n\t\t    padding: 14px;\r\n\t\t    color: #e1e1e1;\r\n\t\t    background: #6A6C75;\r\n\t\t    width: 90%;\r\n\t\t    font-size: 14px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   .fa-search[_ngcontent-%COMP%] {\r\n\t\t    position: relative;\r\n\t\t    left: -25px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\r\n\t\t    padding: 20px;\r\n\t\t    height: 770px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n\t\t    padding-bottom: 20px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n\t\t    float: left;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   .about[_ngcontent-%COMP%] {\r\n\t\t    float: left;\r\n\t\t    margin-top: 8px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   .about[_ngcontent-%COMP%] {\r\n\t\t    padding-left: 8px;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   .status[_ngcontent-%COMP%] {\r\n\t\t    color: #92959E;\r\n\t\t}\r\n\t\t\r\n\t\t.people-list[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\r\n\t\t    color: #92959E;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%] {\r\n\t\t    width: 490px;\r\n\t\t\theight: 800px;\r\n\t\t    float: left;\r\n\t\t    background: #F2F5F8;\r\n\t\t    border-top-right-radius: 5px;\r\n\t\t    border-bottom-right-radius: 5px;\r\n\t\t    color: #434651;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-header[_ngcontent-%COMP%] {\r\n\t\t    padding: 20px;\r\n\t\t    border-bottom: 2px solid white;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-header[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\r\n\t\t    float: left;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-header[_ngcontent-%COMP%]   .chat-about[_ngcontent-%COMP%] {\r\n\t\t    float: left;\r\n\t\t    padding-left: 10px;\r\n\t\t    margin-top: 6px;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-header[_ngcontent-%COMP%]   .chat-with[_ngcontent-%COMP%] {\r\n\t\t    font-weight: bold;\r\n\t\t    font-size: 16px;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-header[_ngcontent-%COMP%]   .chat-num-messages[_ngcontent-%COMP%] {\r\n\t\t    color: #92959E;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-header[_ngcontent-%COMP%]   .fa-star[_ngcontent-%COMP%] {\r\n\t\t    float: right;\r\n\t\t    color: #D8DADF;\r\n\t\t    font-size: 20px;\r\n\t\t    margin-top: 12px;\r\n\t\t}\r\n\t\t\r\n\t\tul[_ngcontent-%COMP%] {\r\n\t\t    list-style-type: none;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%] {\r\n\t\t    padding: 30px 30px 20px;\r\n\t\t    border-bottom: 2px solid white;\r\n\t\t    overflow-y: scroll;\r\n\t\t    height: 575px;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%]   .message-data[_ngcontent-%COMP%] {\r\n\t\t    margin-bottom: 15px;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%]   .message-data-time[_ngcontent-%COMP%] {\r\n\t\t    color: #a8aab1;\r\n\t\t    padding-left: 6px;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%] {\r\n\t\t    color: white;\r\n\t\t    padding: 18px 20px;\r\n\t\t    line-height: 26px;\r\n\t\t    font-size: 16px;\r\n\t\t    border-radius: 7px;\r\n\t\t    margin-bottom: 30px;\r\n\t\t    width: 90%;\r\n\t\t    position: relative;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%]   .message[_ngcontent-%COMP%]:after {\r\n\t\t    bottom: 100%;\r\n\t\t    left: 7%;\r\n\t\t    border: solid transparent;\r\n\t\t    content: \" \";\r\n\t\t    height: 0;\r\n\t\t    width: 0;\r\n\t\t    position: absolute;\r\n\t\t    pointer-events: none;\r\n\t\t    border-bottom-color: #86BB71;\r\n\t\t    border-width: 10px;\r\n\t\t    margin-left: -10px;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%]   .my-message[_ngcontent-%COMP%] {\r\n\t\t    background: #86BB71;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%]   .other-message[_ngcontent-%COMP%] {\r\n\t\t    background: #94C2ED;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-history[_ngcontent-%COMP%]   .other-message[_ngcontent-%COMP%]:after {\r\n\t\t    border-bottom-color: #94C2ED;\r\n\t\t    left: 93%;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-message[_ngcontent-%COMP%] {\r\n\t\t    padding: 30px;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-message[_ngcontent-%COMP%]   textarea[_ngcontent-%COMP%] {\r\n\t\t    width: 100%;\r\n\t\t    border: none;\r\n\t\t    padding: 10px 20px;\r\n\t\t    font: 14px/22px \"Lato\", Arial, sans-serif;\r\n\t\t    margin-bottom: 10px;\r\n\t\t    border-radius: 5px;\r\n\t\t    resize: none;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-message[_ngcontent-%COMP%]   .fa-file-o[_ngcontent-%COMP%], .chat[_ngcontent-%COMP%]   .chat-message[_ngcontent-%COMP%]   .fa-file-image-o[_ngcontent-%COMP%] {\r\n\t\t    font-size: 16px;\r\n\t\t    color: gray;\r\n\t\t    cursor: pointer;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-message[_ngcontent-%COMP%]   button[_ngcontent-%COMP%] {\r\n\t\t    float: right;\r\n\t\t    color: #94C2ED;\r\n\t\t    font-size: 16px;\r\n\t\t    text-transform: uppercase;\r\n\t\t    border: none;\r\n\t\t    cursor: pointer;\r\n\t\t    font-weight: bold;\r\n\t\t    background: #F2F5F8;\r\n\t\t}\r\n\t\t\r\n\t\t.chat[_ngcontent-%COMP%]   .chat-message[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover {\r\n\t\t    color: #75b1e8;\r\n\t\t}\r\n\t\t\r\n\t\t.online[_ngcontent-%COMP%], .offline[_ngcontent-%COMP%], .me[_ngcontent-%COMP%] {\r\n\t\t    margin-right: 3px;\r\n\t\t    font-size: 10px;\r\n\t\t}\r\n\t\t\r\n\t\t.online[_ngcontent-%COMP%] {\r\n\t\t    color: #86BB71;\r\n\t\t}\r\n\t\t\r\n\t\t.offline[_ngcontent-%COMP%] {\r\n\t\t    color: #E38968;\r\n\t\t}\r\n\t\t\r\n\t\t.me[_ngcontent-%COMP%] {\r\n\t\t    color: #94C2ED;\r\n\t\t}\r\n\t\t\r\n\t\t.align-left[_ngcontent-%COMP%] {\r\n\t\t    text-align: left;\r\n\t\t}\r\n\t\t\r\n\t\t.align-right[_ngcontent-%COMP%] {\r\n\t\t    text-align: right;\r\n\t\t}\r\n\t\t\r\n\t\t.float-right[_ngcontent-%COMP%] {\r\n\t\t    float: right;\r\n\t\t}\r\n\t\t\r\n\t\t.clearfix[_ngcontent-%COMP%]:after {\r\n\t\t    visibility: hidden;\r\n\t\t    display: block;\r\n\t\t    font-size: 0;\r\n\t\t    content: \" \";\r\n\t\t    clear: both;\r\n\t\t    height: 0;\r\n\t\t}\r\n\t\t\r\n\t\t.column[_ngcontent-%COMP%] {\r\n            float: left;\r\n            width: 50%;\r\n          }\r\n\t\t\r\n\t\t\r\n\t\t\r\n\t\t.row[_ngcontent-%COMP%]:after {\r\n            content: \"\";\r\n            display: table;\r\n            clear: both;\r\n          }\r\n\t\t\r\n\t\t.my[_ngcontent-%COMP%]{\r\n\t\t\tcolor: cornflowerblue;\r\n\t\t\tbackground-color: #444753;\r\n\t\t\tborder-radius: 5px;\r\n\t\t\ttext-align: right;\r\n\t\t\tmargin: 5px;\r\n\t\t  }\r\n\t\t\r\n\t\tp[_ngcontent-%COMP%]{\r\n\t\t\t  background-color: cornflowerblue;\r\n\t\t\t  border-radius: 5px;\r\n\t\t\t  text-align: left;\r\n\t\t\t  margin: 5px;\r\n\t\t  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNoYXQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztFQUVFO01BQ0ksY0FBYztNQUNkLFlBQVk7R0FDZixhQUFhO01BQ1Ysc0JBQXNCO01BQ3RCLGtCQUFrQjtFQUN0Qjs7RUFFQTtNQUNJLFlBQVk7TUFDWixXQUFXO0VBQ2Y7O0VBRUE7TUFDSSxhQUFhO0VBQ2pCOztFQUVBO01BQ0ksa0JBQWtCO01BQ2xCLFlBQVk7TUFDWixhQUFhO01BQ2IsY0FBYztNQUNkLG1CQUFtQjtNQUNuQixVQUFVO01BQ1YsZUFBZTtFQUNuQjs7RUFFQTtNQUNJLGtCQUFrQjtNQUNsQixXQUFXO0VBQ2Y7O0VBRUE7TUFDSSxhQUFhO01BQ2IsYUFBYTtFQUNqQjs7RUFFQTtNQUNJLG9CQUFvQjtFQUN4Qjs7RUFFQTtNQUNJLFdBQVc7RUFDZjs7RUFFQTtNQUNJLFdBQVc7TUFDWCxlQUFlO0VBQ25COztFQUVBO01BQ0ksaUJBQWlCO0VBQ3JCOztFQUVBO01BQ0ksY0FBYztFQUNsQjs7RUFFQTtNQUNJLGNBQWM7RUFDbEI7O0VBRUE7TUFDSSxZQUFZO0dBQ2YsYUFBYTtNQUNWLFdBQVc7TUFDWCxtQkFBbUI7TUFDbkIsNEJBQTRCO01BQzVCLCtCQUErQjtNQUMvQixjQUFjO0VBQ2xCOztFQUVBO01BQ0ksYUFBYTtNQUNiLDhCQUE4QjtFQUNsQzs7RUFFQTtNQUNJLFdBQVc7RUFDZjs7RUFFQTtNQUNJLFdBQVc7TUFDWCxrQkFBa0I7TUFDbEIsZUFBZTtFQUNuQjs7RUFFQTtNQUNJLGlCQUFpQjtNQUNqQixlQUFlO0VBQ25COztFQUVBO01BQ0ksY0FBYztFQUNsQjs7RUFFQTtNQUNJLFlBQVk7TUFDWixjQUFjO01BQ2QsZUFBZTtNQUNmLGdCQUFnQjtFQUNwQjs7RUFFQTtNQUNJLHFCQUFxQjtFQUN6Qjs7RUFFQTtNQUNJLHVCQUF1QjtNQUN2Qiw4QkFBOEI7TUFDOUIsa0JBQWtCO01BQ2xCLGFBQWE7RUFDakI7O0VBRUE7TUFDSSxtQkFBbUI7RUFDdkI7O0VBRUE7TUFDSSxjQUFjO01BQ2QsaUJBQWlCO0VBQ3JCOztFQUVBO01BQ0ksWUFBWTtNQUNaLGtCQUFrQjtNQUNsQixpQkFBaUI7TUFDakIsZUFBZTtNQUNmLGtCQUFrQjtNQUNsQixtQkFBbUI7TUFDbkIsVUFBVTtNQUNWLGtCQUFrQjtFQUN0Qjs7RUFFQTtNQUNJLFlBQVk7TUFDWixRQUFRO01BQ1IseUJBQXlCO01BQ3pCLFlBQVk7TUFDWixTQUFTO01BQ1QsUUFBUTtNQUNSLGtCQUFrQjtNQUNsQixvQkFBb0I7TUFDcEIsNEJBQTRCO01BQzVCLGtCQUFrQjtNQUNsQixrQkFBa0I7RUFDdEI7O0VBRUE7TUFDSSxtQkFBbUI7RUFDdkI7O0VBRUE7TUFDSSxtQkFBbUI7RUFDdkI7O0VBRUE7TUFDSSw0QkFBNEI7TUFDNUIsU0FBUztFQUNiOztFQUVBO01BQ0ksYUFBYTtFQUNqQjs7RUFFQTtNQUNJLFdBQVc7TUFDWCxZQUFZO01BQ1osa0JBQWtCO01BQ2xCLHlDQUF5QztNQUN6QyxtQkFBbUI7TUFDbkIsa0JBQWtCO01BQ2xCLFlBQVk7RUFDaEI7O0VBRUE7TUFDSSxlQUFlO01BQ2YsV0FBVztNQUNYLGVBQWU7RUFDbkI7O0VBRUE7TUFDSSxZQUFZO01BQ1osY0FBYztNQUNkLGVBQWU7TUFDZix5QkFBeUI7TUFDekIsWUFBWTtNQUNaLGVBQWU7TUFDZixpQkFBaUI7TUFDakIsbUJBQW1CO0VBQ3ZCOztFQUVBO01BQ0ksY0FBYztFQUNsQjs7RUFFQTtNQUNJLGlCQUFpQjtNQUNqQixlQUFlO0VBQ25COztFQUVBO01BQ0ksY0FBYztFQUNsQjs7RUFFQTtNQUNJLGNBQWM7RUFDbEI7O0VBRUE7TUFDSSxjQUFjO0VBQ2xCOztFQUVBO01BQ0ksZ0JBQWdCO0VBQ3BCOztFQUVBO01BQ0ksaUJBQWlCO0VBQ3JCOztFQUVBO01BQ0ksWUFBWTtFQUNoQjs7RUFFQTtNQUNJLGtCQUFrQjtNQUNsQixjQUFjO01BQ2QsWUFBWTtNQUNaLFlBQVk7TUFDWixXQUFXO01BQ1gsU0FBUztFQUNiOztFQUVNO1lBQ0ksV0FBVztZQUNYLFVBQVU7VUFDWjs7RUFFQSxtQ0FBbUM7O0VBQ25DO1lBQ0UsV0FBVztZQUNYLGNBQWM7WUFDZCxXQUFXO1VBQ2I7O0VBRU47R0FDRCxxQkFBcUI7R0FDckIseUJBQXlCO0dBQ3pCLGtCQUFrQjtHQUNsQixpQkFBaUI7R0FDakIsV0FBVztJQUNWOztFQUVBO0tBQ0MsZ0NBQWdDO0tBQ2hDLGtCQUFrQjtLQUNsQixnQkFBZ0I7S0FDaEIsV0FBVztJQUNaIiwiZmlsZSI6ImNoYXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cdFx0XHJcblx0XHQuY29udGFpbmVyIHtcclxuXHRcdCAgICBtYXJnaW46IDAgYXV0bztcclxuXHRcdCAgICB3aWR0aDogNzUwcHg7XHJcblx0XHRcdGhlaWdodDogODAwcHg7XHJcblx0XHQgICAgYmFja2dyb3VuZDogd2hpdGVzbW9rZTtcclxuXHRcdCAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5wZW9wbGUtbGlzdCB7XHJcblx0XHQgICAgd2lkdGg6IDI2MHB4O1xyXG5cdFx0ICAgIGZsb2F0OiBsZWZ0O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQucGVvcGxlLWxpc3QgLnNlYXJjaCB7XHJcblx0XHQgICAgcGFkZGluZzogMjBweDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LnBlb3BsZS1saXN0IGlucHV0IHtcclxuXHRcdCAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcblx0XHQgICAgYm9yZGVyOiBub25lO1xyXG5cdFx0ICAgIHBhZGRpbmc6IDE0cHg7XHJcblx0XHQgICAgY29sb3I6ICNlMWUxZTE7XHJcblx0XHQgICAgYmFja2dyb3VuZDogIzZBNkM3NTtcclxuXHRcdCAgICB3aWR0aDogOTAlO1xyXG5cdFx0ICAgIGZvbnQtc2l6ZTogMTRweDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LnBlb3BsZS1saXN0IC5mYS1zZWFyY2gge1xyXG5cdFx0ICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdCAgICBsZWZ0OiAtMjVweDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LnBlb3BsZS1saXN0IHVsIHtcclxuXHRcdCAgICBwYWRkaW5nOiAyMHB4O1xyXG5cdFx0ICAgIGhlaWdodDogNzcwcHg7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5wZW9wbGUtbGlzdCB1bCBsaSB7XHJcblx0XHQgICAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5wZW9wbGUtbGlzdCBpbWcge1xyXG5cdFx0ICAgIGZsb2F0OiBsZWZ0O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQucGVvcGxlLWxpc3QgLmFib3V0IHtcclxuXHRcdCAgICBmbG9hdDogbGVmdDtcclxuXHRcdCAgICBtYXJnaW4tdG9wOiA4cHg7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5wZW9wbGUtbGlzdCAuYWJvdXQge1xyXG5cdFx0ICAgIHBhZGRpbmctbGVmdDogOHB4O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQucGVvcGxlLWxpc3QgLnN0YXR1cyB7XHJcblx0XHQgICAgY29sb3I6ICM5Mjk1OUU7XHJcblx0XHR9XHJcblxyXG5cdFx0LnBlb3BsZS1saXN0IGgzIHtcclxuXHRcdCAgICBjb2xvcjogIzkyOTU5RTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQge1xyXG5cdFx0ICAgIHdpZHRoOiA0OTBweDtcclxuXHRcdFx0aGVpZ2h0OiA4MDBweDtcclxuXHRcdCAgICBmbG9hdDogbGVmdDtcclxuXHRcdCAgICBiYWNrZ3JvdW5kOiAjRjJGNUY4O1xyXG5cdFx0ICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1cHg7XHJcblx0XHQgICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDVweDtcclxuXHRcdCAgICBjb2xvcjogIzQzNDY1MTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQgLmNoYXQtaGVhZGVyIHtcclxuXHRcdCAgICBwYWRkaW5nOiAyMHB4O1xyXG5cdFx0ICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCB3aGl0ZTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQgLmNoYXQtaGVhZGVyIGltZyB7XHJcblx0XHQgICAgZmxvYXQ6IGxlZnQ7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jaGF0IC5jaGF0LWhlYWRlciAuY2hhdC1hYm91dCB7XHJcblx0XHQgICAgZmxvYXQ6IGxlZnQ7XHJcblx0XHQgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG5cdFx0ICAgIG1hcmdpbi10b3A6IDZweDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQgLmNoYXQtaGVhZGVyIC5jaGF0LXdpdGgge1xyXG5cdFx0ICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdFx0ICAgIGZvbnQtc2l6ZTogMTZweDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQgLmNoYXQtaGVhZGVyIC5jaGF0LW51bS1tZXNzYWdlcyB7XHJcblx0XHQgICAgY29sb3I6ICM5Mjk1OUU7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jaGF0IC5jaGF0LWhlYWRlciAuZmEtc3RhciB7XHJcblx0XHQgICAgZmxvYXQ6IHJpZ2h0O1xyXG5cdFx0ICAgIGNvbG9yOiAjRDhEQURGO1xyXG5cdFx0ICAgIGZvbnQtc2l6ZTogMjBweDtcclxuXHRcdCAgICBtYXJnaW4tdG9wOiAxMnB4O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHR1bCB7XHJcblx0XHQgICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQuY2hhdCAuY2hhdC1oaXN0b3J5IHtcclxuXHRcdCAgICBwYWRkaW5nOiAzMHB4IDMwcHggMjBweDtcclxuXHRcdCAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgd2hpdGU7XHJcblx0XHQgICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG5cdFx0ICAgIGhlaWdodDogNTc1cHg7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jaGF0IC5jaGF0LWhpc3RvcnkgLm1lc3NhZ2UtZGF0YSB7XHJcblx0XHQgICAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQgLmNoYXQtaGlzdG9yeSAubWVzc2FnZS1kYXRhLXRpbWUge1xyXG5cdFx0ICAgIGNvbG9yOiAjYThhYWIxO1xyXG5cdFx0ICAgIHBhZGRpbmctbGVmdDogNnB4O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQuY2hhdCAuY2hhdC1oaXN0b3J5IC5tZXNzYWdlIHtcclxuXHRcdCAgICBjb2xvcjogd2hpdGU7XHJcblx0XHQgICAgcGFkZGluZzogMThweCAyMHB4O1xyXG5cdFx0ICAgIGxpbmUtaGVpZ2h0OiAyNnB4O1xyXG5cdFx0ICAgIGZvbnQtc2l6ZTogMTZweDtcclxuXHRcdCAgICBib3JkZXItcmFkaXVzOiA3cHg7XHJcblx0XHQgICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuXHRcdCAgICB3aWR0aDogOTAlO1xyXG5cdFx0ICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQgLmNoYXQtaGlzdG9yeSAubWVzc2FnZTphZnRlciB7XHJcblx0XHQgICAgYm90dG9tOiAxMDAlO1xyXG5cdFx0ICAgIGxlZnQ6IDclO1xyXG5cdFx0ICAgIGJvcmRlcjogc29saWQgdHJhbnNwYXJlbnQ7XHJcblx0XHQgICAgY29udGVudDogXCIgXCI7XHJcblx0XHQgICAgaGVpZ2h0OiAwO1xyXG5cdFx0ICAgIHdpZHRoOiAwO1xyXG5cdFx0ICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdCAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuXHRcdCAgICBib3JkZXItYm90dG9tLWNvbG9yOiAjODZCQjcxO1xyXG5cdFx0ICAgIGJvcmRlci13aWR0aDogMTBweDtcclxuXHRcdCAgICBtYXJnaW4tbGVmdDogLTEwcHg7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jaGF0IC5jaGF0LWhpc3RvcnkgLm15LW1lc3NhZ2Uge1xyXG5cdFx0ICAgIGJhY2tncm91bmQ6ICM4NkJCNzE7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jaGF0IC5jaGF0LWhpc3RvcnkgLm90aGVyLW1lc3NhZ2Uge1xyXG5cdFx0ICAgIGJhY2tncm91bmQ6ICM5NEMyRUQ7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jaGF0IC5jaGF0LWhpc3RvcnkgLm90aGVyLW1lc3NhZ2U6YWZ0ZXIge1xyXG5cdFx0ICAgIGJvcmRlci1ib3R0b20tY29sb3I6ICM5NEMyRUQ7XHJcblx0XHQgICAgbGVmdDogOTMlO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQuY2hhdCAuY2hhdC1tZXNzYWdlIHtcclxuXHRcdCAgICBwYWRkaW5nOiAzMHB4O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQuY2hhdCAuY2hhdC1tZXNzYWdlIHRleHRhcmVhIHtcclxuXHRcdCAgICB3aWR0aDogMTAwJTtcclxuXHRcdCAgICBib3JkZXI6IG5vbmU7XHJcblx0XHQgICAgcGFkZGluZzogMTBweCAyMHB4O1xyXG5cdFx0ICAgIGZvbnQ6IDE0cHgvMjJweCBcIkxhdG9cIiwgQXJpYWwsIHNhbnMtc2VyaWY7XHJcblx0XHQgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuXHRcdCAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcblx0XHQgICAgcmVzaXplOiBub25lO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQuY2hhdCAuY2hhdC1tZXNzYWdlIC5mYS1maWxlLW8sIC5jaGF0IC5jaGF0LW1lc3NhZ2UgLmZhLWZpbGUtaW1hZ2UtbyB7XHJcblx0XHQgICAgZm9udC1zaXplOiAxNnB4O1xyXG5cdFx0ICAgIGNvbG9yOiBncmF5O1xyXG5cdFx0ICAgIGN1cnNvcjogcG9pbnRlcjtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmNoYXQgLmNoYXQtbWVzc2FnZSBidXR0b24ge1xyXG5cdFx0ICAgIGZsb2F0OiByaWdodDtcclxuXHRcdCAgICBjb2xvcjogIzk0QzJFRDtcclxuXHRcdCAgICBmb250LXNpemU6IDE2cHg7XHJcblx0XHQgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuXHRcdCAgICBib3JkZXI6IG5vbmU7XHJcblx0XHQgICAgY3Vyc29yOiBwb2ludGVyO1xyXG5cdFx0ICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdFx0ICAgIGJhY2tncm91bmQ6ICNGMkY1Rjg7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jaGF0IC5jaGF0LW1lc3NhZ2UgYnV0dG9uOmhvdmVyIHtcclxuXHRcdCAgICBjb2xvcjogIzc1YjFlODtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Lm9ubGluZSwgLm9mZmxpbmUsIC5tZSB7XHJcblx0XHQgICAgbWFyZ2luLXJpZ2h0OiAzcHg7XHJcblx0XHQgICAgZm9udC1zaXplOiAxMHB4O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQub25saW5lIHtcclxuXHRcdCAgICBjb2xvcjogIzg2QkI3MTtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0Lm9mZmxpbmUge1xyXG5cdFx0ICAgIGNvbG9yOiAjRTM4OTY4O1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQubWUge1xyXG5cdFx0ICAgIGNvbG9yOiAjOTRDMkVEO1xyXG5cdFx0fVxyXG5cdFx0XHJcblx0XHQuYWxpZ24tbGVmdCB7XHJcblx0XHQgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmFsaWduLXJpZ2h0IHtcclxuXHRcdCAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuXHRcdH1cclxuXHRcdFxyXG5cdFx0LmZsb2F0LXJpZ2h0IHtcclxuXHRcdCAgICBmbG9hdDogcmlnaHQ7XHJcblx0XHR9XHJcblx0XHRcclxuXHRcdC5jbGVhcmZpeDphZnRlciB7XHJcblx0XHQgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG5cdFx0ICAgIGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0ICAgIGZvbnQtc2l6ZTogMDtcclxuXHRcdCAgICBjb250ZW50OiBcIiBcIjtcclxuXHRcdCAgICBjbGVhcjogYm90aDtcclxuXHRcdCAgICBoZWlnaHQ6IDA7XHJcblx0XHR9XHJcblxyXG4gICAgICAgIC5jb2x1bW4ge1xyXG4gICAgICAgICAgICBmbG9hdDogbGVmdDtcclxuICAgICAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgLyogQ2xlYXIgZmxvYXRzIGFmdGVyIHRoZSBjb2x1bW5zICovXHJcbiAgICAgICAgICAucm93OmFmdGVyIHtcclxuICAgICAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICAgICAgZGlzcGxheTogdGFibGU7XHJcbiAgICAgICAgICAgIGNsZWFyOiBib3RoO1xyXG4gICAgICAgICAgfVxyXG5cclxuXHRcdCAgLm15e1xyXG5cdFx0XHRjb2xvcjogY29ybmZsb3dlcmJsdWU7XHJcblx0XHRcdGJhY2tncm91bmQtY29sb3I6ICM0NDQ3NTM7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDVweDtcclxuXHRcdFx0dGV4dC1hbGlnbjogcmlnaHQ7XHJcblx0XHRcdG1hcmdpbjogNXB4O1xyXG5cdFx0ICB9XHJcblxyXG5cdFx0ICBwe1xyXG5cdFx0XHQgIGJhY2tncm91bmQtY29sb3I6IGNvcm5mbG93ZXJibHVlO1xyXG5cdFx0XHQgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuXHRcdFx0ICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG5cdFx0XHQgIG1hcmdpbjogNXB4O1xyXG5cdFx0ICB9Il19 */"] });


/***/ }),

/***/ "FPeM":
/*!*******************************************************************!*\
  !*** ./src/app/component/nav-bar/connection/connection.models.ts ***!
  \*******************************************************************/
/*! exports provided: ConnectionDto, Connection */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionDto", function() { return ConnectionDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Connection", function() { return Connection; });
class ConnectionDto {
    constructor() {
        this.name = '';
        this.url = '';
        this.token = '';
        this.project = '';
        this.branchName = '';
    }
    deserialize(input) {
        return Object.assign(this, input);
    }
}
class Connection {
    constructor() {
        this.id = 0;
        this.name = '';
        this.url = '';
        this.token = '';
        this.project = '';
        this.branchName = '';
    }
    deserialize(input) {
        return Object.assign(this, input);
    }
}


/***/ }),

/***/ "H5eL":
/*!******************************************************!*\
  !*** ./src/app/component/service/graphql.service.ts ***!
  \******************************************************/
/*! exports provided: GraphqlService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GraphqlService", function() { return GraphqlService; });
/* harmony import */ var src_app_util_paths__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/util/paths */ "9ZzF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class GraphqlService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getAllGraphQL(query) {
        return this.httpClient.post(src_app_util_paths__WEBPACK_IMPORTED_MODULE_0__["GRAPHQL_PATH"], query);
    }
}
GraphqlService.ɵfac = function GraphqlService_Factory(t) { return new (t || GraphqlService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
GraphqlService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: GraphqlService, factory: GraphqlService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "Hc/g":
/*!*************************************************!*\
  !*** ./src/app/component/login/login.models.ts ***!
  \*************************************************/
/*! exports provided: UserDto, User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDto", function() { return UserDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
class UserDto {
    constructor() {
        this.email = '';
        this.password = '';
    }
    deserialize(input) {
        return Object.assign(this, input);
    }
}
class User {
    constructor() {
        this.id = 0;
        this.email = '';
        this.password = '';
    }
    deserialize(input) {
        return Object.assign(this, input);
    }
}


/***/ }),

/***/ "S2BP":
/*!**********************************************************************!*\
  !*** ./src/app/component/nav-bar/connection/connection.component.ts ***!
  \**********************************************************************/
/*! exports provided: ConnectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionComponent", function() { return ConnectionComponent; });
/* harmony import */ var src_app_component_nav_bar_connection_connection_models__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/component/nav-bar/connection/connection.models */ "FPeM");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _service_connection_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../service/connection.service */ "njQe");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../login/login.service */ "6x5B");
/* harmony import */ var _service_graphql_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/graphql.service */ "H5eL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");









function ConnectionComponent_div_0_div_9_p_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Name: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](connection_r3.name);
} }
function ConnectionComponent_div_0_div_9_input_7_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_0_div_9_input_7_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r13); const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit; return connection_r3.name = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", connection_r3.name);
} }
function ConnectionComponent_div_0_div_9_div_8_p_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Url: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](connection_r3.url);
} }
function ConnectionComponent_div_0_div_9_div_8_input_2_Template(rf, ctx) { if (rf & 1) {
    const _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_0_div_9_div_8_input_2_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r26); const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return connection_r3.url = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", connection_r3.url);
} }
function ConnectionComponent_div_0_div_9_div_8_p_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Token: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](connection_r3.token);
} }
function ConnectionComponent_div_0_div_9_div_8_input_4_Template(rf, ctx) { if (rf & 1) {
    const _r31 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_0_div_9_div_8_input_4_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r31); const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return connection_r3.token = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", connection_r3.token);
} }
function ConnectionComponent_div_0_div_9_div_8_p_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Project: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](connection_r3.project);
} }
function ConnectionComponent_div_0_div_9_div_8_input_6_Template(rf, ctx) { if (rf & 1) {
    const _r36 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_0_div_9_div_8_input_6_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r36); const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return connection_r3.project = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", connection_r3.project);
} }
function ConnectionComponent_div_0_div_9_div_8_p_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Branch name: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](connection_r3.branchName);
} }
function ConnectionComponent_div_0_div_9_div_8_input_8_Template(rf, ctx) { if (rf & 1) {
    const _r41 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "input", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_0_div_9_div_8_input_8_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r41); const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit; return connection_r3.branchName = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", connection_r3.branchName);
} }
function ConnectionComponent_div_0_div_9_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ConnectionComponent_div_0_div_9_div_8_p_1_Template, 4, 1, "p", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ConnectionComponent_div_0_div_9_div_8_input_2_Template, 1, 1, "input", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, ConnectionComponent_div_0_div_9_div_8_p_3_Template, 4, 1, "p", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, ConnectionComponent_div_0_div_9_div_8_input_4_Template, 1, 1, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, ConnectionComponent_div_0_div_9_div_8_p_5_Template, 4, 1, "p", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ConnectionComponent_div_0_div_9_div_8_input_6_Template, 1, 1, "input", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ConnectionComponent_div_0_div_9_div_8_p_7_Template, 4, 1, "p", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, ConnectionComponent_div_0_div_9_div_8_input_8_Template, 1, 1, "input", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const i_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().index;
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEditIndex != i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEdit && ctx_r7.enableEditIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEditIndex != i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEdit && ctx_r7.enableEditIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEditIndex != i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEdit && ctx_r7.enableEditIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEditIndex != i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r7.enableEdit && ctx_r7.enableEditIndex == i_r4);
} }
function ConnectionComponent_div_0_div_9_div_10_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r50 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_0_div_9_div_10_button_1_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r50); const i_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).index; const ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r48.save(i_r4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_0_div_9_div_10_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r53 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_0_div_9_div_10_button_2_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r53); const i_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).index; const ctx_r51 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r51.cancel(i_r4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_0_div_9_div_10_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r56 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_0_div_9_div_10_button_3_Template_button_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r56); const i_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).index; const ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r54.enableEditMethod($event, i_r4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Edit");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_0_div_9_div_10_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r59 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_0_div_9_div_10_button_4_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r59); const i_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2).index; const ctx_r57 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r57.delete(i_r4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Delete");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_0_div_9_div_10_Template(rf, ctx) { if (rf & 1) {
    const _r61 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ConnectionComponent_div_0_div_9_div_10_button_1_Template, 3, 0, "button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, ConnectionComponent_div_0_div_9_div_10_button_2_Template, 3, 0, "button", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, ConnectionComponent_div_0_div_9_div_10_button_3_Template, 3, 0, "button", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, ConnectionComponent_div_0_div_9_div_10_button_4_Template, 3, 0, "button", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "button", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_0_div_9_div_10_Template_button_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r61); const ctx_r60 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](3); return ctx_r60.showLess(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "i", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, " Show less");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const i_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().index;
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r8.enableEdit && ctx_r8.enableEditIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r8.enableEdit && ctx_r8.enableEditIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r8.enableEdit);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r8.enableEdit);
} }
function ConnectionComponent_div_0_div_9_button_11_Template(rf, ctx) { if (rf & 1) {
    const _r65 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_0_div_9_button_11_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r65); const i_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().index; const ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2); return ctx_r63.showMore(i_r4); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "i", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, " Show more");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_0_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "h5", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Connection ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, ConnectionComponent_div_0_div_9_p_6_Template, 4, 1, "p", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, ConnectionComponent_div_0_div_9_input_7_Template, 1, 1, "input", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, ConnectionComponent_div_0_div_9_div_8_Template, 9, 8, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](10, ConnectionComponent_div_0_div_9_div_10_Template, 8, 4, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](11, ConnectionComponent_div_0_div_9_button_11_Template, 3, 0, "button", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const connection_r3 = ctx.$implicit;
    const i_r4 = ctx.index;
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](connection_r3.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.enableEditIndex != i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.enableEdit && ctx_r2.enableEditIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.enableShowMore && ctx_r2.enableShowIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.enableShowMore && ctx_r2.enableShowIndex == i_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx_r2.enableShowMore || ctx_r2.enableShowIndex != i_r4);
} }
function ConnectionComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    const _r67 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Connections");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "button", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_0_Template_button_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r67); const ctx_r66 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r66.enableCreate(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "i", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Create connection");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](9, ConnectionComponent_div_0_div_9_Template, 12, 6, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r0.connections);
} }
function ConnectionComponent_div_1_div_12_small_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "small", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Wrong url.");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_1_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "small", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Url is required field.");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, ConnectionComponent_div_1_div_12_small_3_Template, 2, 0, "small", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    const _r69 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r69.errors.pattern);
} }
function ConnectionComponent_div_1_div_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "small", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Project name is required field.");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_1_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "small", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Branch is required field.");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ConnectionComponent_div_1_Template(rf, ctx) { if (rf & 1) {
    const _r77 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "form", 40, 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Create connection");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "input", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_1_Template_input_ngModelChange_8_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77); const ctx_r76 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r76.newConnection.name = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "input", 43, 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_1_Template_input_ngModelChange_10_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77); const ctx_r78 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r78.newConnection.url = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, ConnectionComponent_div_1_div_12_Template, 4, 1, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](13, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "input", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_1_Template_input_ngModelChange_14_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77); const ctx_r79 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r79.newConnection.token = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](15, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "input", 46, 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_1_Template_input_ngModelChange_16_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77); const ctx_r80 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r80.newConnection.project = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, ConnectionComponent_div_1_div_18_Template, 3, 0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](19, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "input", 48, 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function ConnectionComponent_div_1_Template_input_ngModelChange_20_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77); const ctx_r81 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r81.newConnection.branchName = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](22, ConnectionComponent_div_1_div_22_Template, 3, 0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](23, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "button", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_1_Template_button_click_24_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77); const ctx_r82 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r82.create(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "button", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ConnectionComponent_div_1_Template_button_click_26_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r77); const ctx_r83 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r83.cancelCreate(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const _r68 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](4);
    const _r69 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](11);
    const _r71 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](17);
    const _r73 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](21);
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.newConnection.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("is-invalid", _r69.invalid && _r69.touched);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.newConnection.url);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r69.errors && (_r69.invalid && _r69.touched));
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.newConnection.token);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("is-invalid", _r71.touched && _r71.invalid);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.newConnection.project);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r71.touched && _r71.invalid);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("is-invalid", _r73.touched && _r73.invalid);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.newConnection.branchName);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r73.touched && _r73.invalid);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", _r68.form.invalid);
} }
class ConnectionComponent {
    constructor(connectionService, toastr, router, loginService, graphQL) {
        this.connectionService = connectionService;
        this.toastr = toastr;
        this.router = router;
        this.loginService = loginService;
        this.graphQL = graphQL;
        this.enableEdit = false;
        this.enableEditIndex = null;
        this.enableShowIndex = null;
        this.addConnection = false;
        this.newConnection = new src_app_component_nav_bar_connection_connection_models__WEBPACK_IMPORTED_MODULE_0__["ConnectionDto"]();
        this.query = '';
        this.enableShowMore = false;
    }
    ngOnInit() {
        if (!this.loginService.isUserLogin()) {
            this.router.navigate(['login']);
        }
        this.query = '{ connections { id name } }';
        this.graphQL.getAllGraphQL(this.query).subscribe((connectionList) => {
            this.connections = connectionList.data.connections;
        }, (err) => {
            this.toastr.info('There is no connection. ' + err.error.message);
        });
    }
    enableEditMethod(e, i) {
        this.prevConnection = this.connections[i];
        this.enableEdit = true;
        this.enableEditIndex = i;
    }
    save(i) {
        this.enableEdit = false;
        this.connectionService.edit(this.connections[i]).subscribe((response) => {
            this.connections[i] = new src_app_component_nav_bar_connection_connection_models__WEBPACK_IMPORTED_MODULE_0__["Connection"]().deserialize(response);
            this.enableEditIndex = null;
            this.toastr.success('Changed ' + response.name + ' connection.');
        }, (err) => {
            this.toastr.error('Changing ' + this.connections[i].name + ' connection failed. ' + err.error.message);
        });
    }
    cancel(i) {
        this.enableEditIndex = null;
        this.connections[i] = this.prevConnection;
        this.enableEdit = false;
    }
    delete(i) {
        this.connectionService.delete(this.connections[i].id).subscribe((response) => {
            this.toastr.success('Deleted connection.');
            window.location.reload();
        }, (err) => {
            this.toastr.error('Deleting connection is not possible. ' + err.error.message);
        });
    }
    create() {
        this.connectionService.create(this.newConnection).subscribe((response) => {
            this.addConnection = false;
            window.location.reload();
            this.toastr.success('Connection created.');
        }, (err) => {
            this.toastr.error('Creating ' + this.newConnection.name + ' connection failed. ' + err.error.message);
        });
    }
    enableCreate() {
        this.addConnection = true;
    }
    cancelCreate() {
        this.addConnection = false;
    }
    showMore(i) {
        this.enableShowMore = true;
        this.enableShowIndex = i;
        this.query = '{ connection(name: "' + this.connections[i].name + '") {id name url token project branchName} }';
        this.graphQL.getAllGraphQL(this.query).subscribe((response) => {
            this.connections[i] = response.data.connection;
        });
    }
    showLess() {
        this.enableEdit = false;
        this.enableEditIndex = null;
        this.addConnection = false;
        this.enableShowMore = false;
    }
}
ConnectionComponent.ɵfac = function ConnectionComponent_Factory(t) { return new (t || ConnectionComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_connection_service__WEBPACK_IMPORTED_MODULE_2__["ConnectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_login_login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_service_graphql_service__WEBPACK_IMPORTED_MODULE_6__["GraphqlService"])); };
ConnectionComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ConnectionComponent, selectors: [["app-connection"]], decls: 2, vars: 2, consts: [["class", "banner", 4, "ngIf"], [1, "banner"], ["id", "create", 1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-add"], [1, "cards"], ["class", "card", 4, "ngFor", "ngForOf"], [1, "card"], [1, "card-title"], [1, "card-body"], [4, "ngIf"], ["class", "form-control", "type", "text", "id", "name", "required", "", 3, "ngModel", "ngModelChange", 4, "ngIf"], [1, "buttons"], ["class", "btn btn-info", "id", "showMoreBtn", 3, "click", 4, "ngIf"], ["type", "text", "id", "name", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["class", "form-control", "type", "text", "id", "url", "required", "", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "form-control", "type", "text", "id", "token", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "form-control", "type", "text", "id", "project", "required", "", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["class", "form-control", "type", "text", "id", "branchName", "required", "", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["type", "text", "id", "url", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "token", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "project", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "branchName", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["class", "btn btn-info", "id", "editBtn", 3, "click", 4, "ngIf"], ["class", "btn btn-secondary", "id", "editBtn", 3, "click", 4, "ngIf"], ["class", "btn btn-info", "id", "edit", 3, "click", 4, "ngIf"], ["class", "btn btn-secondary", "id", "delete", 3, "click", 4, "ngIf"], [1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-caret-up"], ["id", "editBtn", 1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-save"], ["id", "editBtn", 1, "btn", "btn-secondary", 3, "click"], [1, "fa", "fa-times"], ["id", "edit", 1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-edit"], ["id", "delete", 1, "btn", "btn-secondary", 3, "click"], [1, "fa", "fa-trash-o"], ["id", "showMoreBtn", 1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-caret-down"], [1, "card-add-con"], [1, "inputs"], [1, "ngform"], ["addForm", "ngForm"], ["type", "text", "id", "nameNew", "name", "nameNew", "placeholder", "Enter connection name", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "urlNew", "name", "urlNew", "placeholder", "Enter url", "required", "", "pattern", "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})", 1, "form-control", 3, "ngModel", "ngModelChange"], ["urlNew", "ngModel"], ["type", "text", "id", "tokenNew", "name", "tokenNew", "placeholder", "Enter token", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "projectNew", "name", "projectNew", "placeholder", "Enter project name", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["projectNew", "ngModel"], ["type", "text", "id", "branchNameNew", "name", "branchNameNew", "placeholder", "Enter branch name", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["branchNameNew", "ngModel"], [1, "btn", "btn-info", "form-control", 3, "disabled", "click"], [1, "btn", "btn-secondary", "form-control", 3, "click"], [1, "text-danger"], ["class", "text-danger", 4, "ngIf"]], template: function ConnectionComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, ConnectionComponent_div_0_Template, 10, 1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ConnectionComponent_div_1_Template, 28, 15, "div", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.addConnection);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.addConnection);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["PatternValidator"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb25uZWN0aW9uLmNvbXBvbmVudC5jc3MifQ== */"] });


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _component_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./component/nav-bar/nav-bar.component */ "ldvO");


class AppComponent {
    constructor() {
        this.title = 'Frontend';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 7, vars: 0, consts: [["id", "footer", 1, "copy-right"], ["href", "http://majastamenic.herokuapp.com/", "target", "_blank"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-nav-bar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "\u00A9 ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Maja Stameni\u0107");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " - QaCube 2021");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_component_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_1__["NavBarComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LmNzcyJ9 */"] });


/***/ }),

/***/ "U+dW":
/*!**********************************************************!*\
  !*** ./src/app/component/service/application.service.ts ***!
  \**********************************************************/
/*! exports provided: ApplicationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationService", function() { return ApplicationService; });
/* harmony import */ var _util_paths__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../util/paths */ "9ZzF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class ApplicationService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    create(connection) {
        return this.httpClient.post(_util_paths__WEBPACK_IMPORTED_MODULE_0__["APPLICATION_PATH"], connection);
    }
    getAll() {
        return this.httpClient.get(_util_paths__WEBPACK_IMPORTED_MODULE_0__["APPLICATION_PATH"]);
    }
    edit(connection) {
        return this.httpClient.put(_util_paths__WEBPACK_IMPORTED_MODULE_0__["APPLICATION_PATH"], connection);
    }
    delete(id) {
        return this.httpClient.delete(_util_paths__WEBPACK_IMPORTED_MODULE_0__["APPLICATION_PATH"] + '/' + id);
    }
}
ApplicationService.ɵfac = function ApplicationService_Factory(t) { return new (t || ApplicationService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
ApplicationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: ApplicationService, factory: ApplicationService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "XUBd":
/*!************************************************************************!*\
  !*** ./src/app/component/nav-bar/application/application.component.ts ***!
  \************************************************************************/
/*! exports provided: ApplicationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationComponent", function() { return ApplicationComponent; });
/* harmony import */ var _application_models__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./application.models */ "qNlj");
/* harmony import */ var _connection_connection_models__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../connection/connection.models */ "FPeM");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var src_app_component_service_application_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/component/service/application.service */ "U+dW");
/* harmony import */ var src_app_component_service_connection_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/component/service/connection.service */ "njQe");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_component_login_login_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/component/login/login.service */ "6x5B");
/* harmony import */ var _service_graphql_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../service/graphql.service */ "H5eL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-select/ng-select */ "ZOsW");












function ApplicationComponent_div_9_p_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Name: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const application_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](application_r1.name);
} }
function ApplicationComponent_div_9_input_7_Template(rf, ctx) { if (rf & 1) {
    const _r11 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "input", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function ApplicationComponent_div_9_input_7_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r11); const application_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit; return application_r1.name = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const application_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngModel", application_r1.name);
} }
function ApplicationComponent_div_9_div_8_p_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Connections: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ApplicationComponent_div_9_div_8_ol_2_li_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](2, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const con_r17 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](con_r17.name);
} }
function ApplicationComponent_div_9_div_8_ol_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "ol");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ApplicationComponent_div_9_div_8_ol_2_li_1_Template, 3, 1, "li", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const application_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", application_r1.connections);
} }
function ApplicationComponent_div_9_div_8_ng_select_4_ng_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "ng-option", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const conn_r20 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("value", conn_r20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](conn_r20.name);
} }
const _c0 = function () { return { standalone: true }; };
function ApplicationComponent_div_9_div_8_ng_select_4_Template(rf, ctx) { if (rf & 1) {
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "ng-select", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function ApplicationComponent_div_9_div_8_ng_select_4_Template_ng_select_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r23); const application_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).$implicit; return application_r1.connections = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ApplicationComponent_div_9_div_8_ng_select_4_ng_option_1_Template, 2, 2, "ng-option", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const application_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).$implicit;
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngModel", application_r1.connections)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction0"](5, _c0))("multiple", true)("searchable", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx_r15.connections);
} }
function ApplicationComponent_div_9_div_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ApplicationComponent_div_9_div_8_p_1_Template, 2, 0, "p", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, ApplicationComponent_div_9_div_8_ol_2_Template, 2, 1, "ol", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](4, ApplicationComponent_div_9_div_8_ng_select_4_Template, 2, 6, "ng-select", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const i_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().index;
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r5.enableEditIndex != i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r5.enableEditIndex != i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r5.enableEdit && ctx_r5.enableEditIndex == i_r2);
} }
function ApplicationComponent_div_9_div_10_button_1_Template(rf, ctx) { if (rf & 1) {
    const _r32 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "button", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ApplicationComponent_div_9_div_10_button_1_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r32); const i_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).index; const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r30.saveApp(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ApplicationComponent_div_9_div_10_button_2_Template(rf, ctx) { if (rf & 1) {
    const _r35 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "button", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ApplicationComponent_div_9_div_10_button_2_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r35); const i_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).index; const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r33.cancelApp(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ApplicationComponent_div_9_div_10_button_3_Template(rf, ctx) { if (rf & 1) {
    const _r38 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ApplicationComponent_div_9_div_10_button_3_Template_button_click_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r38); const i_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).index; const ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r36.enableEditMethodApp($event, i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Edit");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ApplicationComponent_div_9_div_10_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r41 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ApplicationComponent_div_9_div_10_button_4_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r41); const i_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2).index; const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r39.deleteApp(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Delete");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ApplicationComponent_div_9_div_10_Template(rf, ctx) { if (rf & 1) {
    const _r43 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ApplicationComponent_div_9_div_10_button_1_Template, 3, 0, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, ApplicationComponent_div_9_div_10_button_2_Template, 3, 0, "button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, ApplicationComponent_div_9_div_10_button_3_Template, 3, 0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](4, ApplicationComponent_div_9_div_10_button_4_Template, 3, 0, "button", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "button", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ApplicationComponent_div_9_div_10_Template_button_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r43); const ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2); return ctx_r42.showLess(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](6, "i", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, " Show less");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const i_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().index;
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r6.enableEdit && ctx_r6.enableEditIndex == i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r6.enableEdit && ctx_r6.enableEditIndex == i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !ctx_r6.enableEdit);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !ctx_r6.enableEdit);
} }
function ApplicationComponent_div_9_button_11_Template(rf, ctx) { if (rf & 1) {
    const _r47 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "button", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ApplicationComponent_div_9_button_11_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r47); const i_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().index; const ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r45.showMore(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Show more");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ApplicationComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h5", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Application ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](6, ApplicationComponent_div_9_p_6_Template, 4, 1, "p", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](7, ApplicationComponent_div_9_input_7_Template, 1, 1, "input", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](8, ApplicationComponent_div_9_div_8_Template, 5, 3, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](10, ApplicationComponent_div_9_div_10_Template, 8, 4, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](11, ApplicationComponent_div_9_button_11_Template, 3, 0, "button", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const application_r1 = ctx.$implicit;
    const i_r2 = ctx.index;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](application_r1.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.enableEditIndex != i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.enableEdit && ctx_r0.enableEditIndex == i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.enableShowMore && ctx_r0.enableShowIndex == i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.enableShowMore && ctx_r0.enableShowIndex == i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !ctx_r0.enableShowMore || ctx_r0.enableShowIndex != i_r2);
} }
class ApplicationComponent {
    constructor(toastr, applicationService, connectionService, router, loginService, graphQL) {
        this.toastr = toastr;
        this.applicationService = applicationService;
        this.connectionService = connectionService;
        this.router = router;
        this.loginService = loginService;
        this.graphQL = graphQL;
        this.enableEdit = false;
        this.enableEditIndex = null;
        this.query = '';
        this.enableShowMore = false;
        this.enableShowIndex = null;
        this.connections = [];
    }
    ngOnInit() {
        if (!this.loginService.isUserLogin()) {
            this.router.navigate(['login']);
        }
        this.query = '{ applications {id name } }';
        this.graphQL.getAllGraphQL(this.query).subscribe((response) => {
            this.applications = response.data.applications;
        }, (err) => {
            this.toastr.info('There is no application. ' + err.error.message);
        });
        this.connectionService.getAll().subscribe((response) => {
            for (const con of response) {
                this.connections.push(new _connection_connection_models__WEBPACK_IMPORTED_MODULE_1__["Connection"]().deserialize(con));
            }
        });
    }
    enableEditMethodApp(e, i) {
        this.prevApplication = this.applications[i];
        this.enableEdit = true;
        this.enableEditIndex = i;
    }
    saveApp(i) {
        this.enableEdit = false;
        this.applicationService.edit(this.applications[i]).subscribe((response) => {
            this.applications[i] = new _application_models__WEBPACK_IMPORTED_MODULE_0__["Application"]().deserialize(response);
            this.enableEditIndex = null;
            this.toastr.success('Changed ' + response.name + ' application.');
        }, (err) => {
            this.toastr.error('Changing ' + this.applications[i].name + ' application failed. ' + err.error.message);
        });
    }
    cancelApp(i) {
        this.enableEditIndex = null;
        this.applications[i] = this.prevApplication;
        this.enableEdit = false;
    }
    deleteApp(i) {
        this.applicationService.delete(this.applications[i].id).subscribe((response) => {
            this.toastr.success('Deleted application.');
            window.location.reload();
        }, (err) => {
            this.toastr.error('Deleting application is not possible. ' + err.error.message);
        });
    }
    showMore(i) {
        this.enableShowMore = true;
        this.enableShowIndex = i;
        this.query = '{ application(name: "' + this.applications[i].name + '") {id name connections{id name url token project branchName}} }';
        this.graphQL.getAllGraphQL(this.query).subscribe((response) => {
            this.applications[i] = response.data.application;
        });
    }
    showLess() {
        this.enableEdit = false;
        this.enableEditIndex = null;
        this.enableShowMore = false;
    }
}
ApplicationComponent.ɵfac = function ApplicationComponent_Factory(t) { return new (t || ApplicationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_component_service_application_service__WEBPACK_IMPORTED_MODULE_4__["ApplicationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_component_service_connection_service__WEBPACK_IMPORTED_MODULE_5__["ConnectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_component_login_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_service_graphql_service__WEBPACK_IMPORTED_MODULE_8__["GraphqlService"])); };
ApplicationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: ApplicationComponent, selectors: [["app-application"]], decls: 10, vars: 1, consts: [[1, "banner"], ["id", "create", "routerLink", "/navbar/application/add", 1, "btn", "btn-info"], [1, "fa", "fa-add"], [1, "cards"], ["class", "card", 4, "ngFor", "ngForOf"], [1, "card"], [1, "card-title"], [1, "card-body"], [4, "ngIf"], ["class", "form-control", "type", "text", "id", "name", "required", "", 3, "ngModel", "ngModelChange", 4, "ngIf"], [1, "buttons"], ["class", "btn btn-info", "id", "showMoreBtn", 3, "click", 4, "ngIf"], ["type", "text", "id", "name", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["placeholder", "Chose connections", "bindLabel", "name", "appendTo", "body", 3, "ngModel", "ngModelOptions", "multiple", "searchable", "ngModelChange", 4, "ngIf"], [4, "ngFor", "ngForOf"], ["placeholder", "Chose connections", "bindLabel", "name", "appendTo", "body", 3, "ngModel", "ngModelOptions", "multiple", "searchable", "ngModelChange"], ["name", "name", 3, "value", 4, "ngFor", "ngForOf"], ["name", "name", 3, "value"], ["class", "btn btn-info", "id", "saveBtn", 3, "click", 4, "ngIf"], ["class", "btn btn-secondary", "id", "cancelBtn", 3, "click", 4, "ngIf"], ["class", "btn btn-info", "id", "edit", 3, "click", 4, "ngIf"], ["class", "btn btn-secondary", "id", "delete", 3, "click", 4, "ngIf"], [1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-caret-up"], ["id", "saveBtn", 1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-save"], ["id", "cancelBtn", 1, "btn", "btn-secondary", 3, "click"], [1, "fa", "fa-times"], ["id", "edit", 1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-edit"], ["id", "delete", 1, "btn", "btn-secondary", 3, "click"], [1, "fa", "fa-trash-o"], ["id", "showMoreBtn", 1, "btn", "btn-info", 3, "click"], [1, "fa", "fa-caret-down"]], template: function ApplicationComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Applications");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](5, "i", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, "Create application");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](7, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](9, ApplicationComponent_div_9_Template, 12, 6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx.applications);
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgModel"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["NgSelectComponent"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_11__["ɵr"]], styles: ["button[_ngcontent-%COMP%] {\r\n    margin-top: 15px;\r\n}\r\n\r\nng-multiselect-dropdown[_ngcontent-%COMP%] {\r\n    background-color: white;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcGxpY2F0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSx1QkFBdUI7QUFDM0IiLCJmaWxlIjoiYXBwbGljYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbImJ1dHRvbiB7XHJcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG59XHJcblxyXG5uZy1tdWx0aXNlbGVjdC1kcm9wZG93biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufSJdfQ== */"] });


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-select/ng-select */ "ZOsW");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _component_nav_bar_connection_connection_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./component/nav-bar/connection/connection.component */ "S2BP");
/* harmony import */ var _component_login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./component/login/login.component */ "g8H4");
/* harmony import */ var _component_nav_bar_application_add_application_add_application_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./component/nav-bar/application/add-application/add-application.component */ "prkM");
/* harmony import */ var _component_nav_bar_application_application_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./component/nav-bar/application/application.component */ "XUBd");
/* harmony import */ var _component_nav_bar_commit_commit_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./component/nav-bar/commit/commit.component */ "qRRk");
/* harmony import */ var _component_nav_bar_home_home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./component/nav-bar/home/home.component */ "ataK");
/* harmony import */ var _component_nav_bar_merge_request_merge_request_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./component/nav-bar/merge-request/merge-request.component */ "oCnx");
/* harmony import */ var _component_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./component/nav-bar/nav-bar.component */ "ldvO");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! angularx-social-login */ "ahC7");
/* harmony import */ var _component_nav_bar_chat_chat_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./component/nav-bar/chat/chat.component */ "EJ1J");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/core */ "fXoL");
















// social login





class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_18__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_18__["ɵɵdefineInjector"]({ providers: [
        {
            provide: 'SocialAuthServiceConfig',
            useValue: {
                autoLogin: false,
                providers: [
                    {
                        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_16__["GoogleLoginProvider"].PROVIDER_ID,
                        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_16__["GoogleLoginProvider"]('857613503157-4kkeerf5qmku0r7edlvmk57f73d92ht1.apps.googleusercontent.com')
                    },
                    {
                        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_16__["FacebookLoginProvider"].PROVIDER_ID,
                        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_16__["FacebookLoginProvider"]('385064976163664')
                    }
                ]
            },
        }
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrModule"].forRoot({
                timeOut: 10000,
                positionClass: 'toast-bottom-right',
                preventDuplicates: true,
            }),
            _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_4__["NgSelectModule"],
            angularx_social_login__WEBPACK_IMPORTED_MODULE_16__["SocialLoginModule"],
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_18__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _component_nav_bar_merge_request_merge_request_component__WEBPACK_IMPORTED_MODULE_14__["MergeRequestComponent"],
        _component_nav_bar_application_application_component__WEBPACK_IMPORTED_MODULE_11__["ApplicationComponent"],
        _component_nav_bar_connection_connection_component__WEBPACK_IMPORTED_MODULE_8__["ConnectionComponent"],
        _component_nav_bar_commit_commit_component__WEBPACK_IMPORTED_MODULE_12__["CommitComponent"],
        _component_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_15__["NavBarComponent"],
        _component_nav_bar_home_home_component__WEBPACK_IMPORTED_MODULE_13__["HomeComponent"],
        _component_nav_bar_application_add_application_add_application_component__WEBPACK_IMPORTED_MODULE_10__["AddApplicationComponent"],
        _component_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"],
        _component_nav_bar_chat_chat_component__WEBPACK_IMPORTED_MODULE_17__["ChatComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClientModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrModule"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_4__["NgSelectModule"],
        angularx_social_login__WEBPACK_IMPORTED_MODULE_16__["SocialLoginModule"]] }); })();


/***/ }),

/***/ "ataK":
/*!**********************************************************!*\
  !*** ./src/app/component/nav-bar/home/home.component.ts ***!
  \**********************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _service_web_socket_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../service/web-socket.service */ "1JXD");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../login/login.service */ "6x5B");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "5eHb");





const WEBSOCKET_URL = 'ws://localhost:8080/socket';
const EXAMPLE_URL = '/topic/server-broadcaster';
class HomeComponent {
    constructor(loginService, router, toastr) {
        this.loginService = loginService;
        this.router = router;
        this.toastr = toastr;
        this.webSocketService = new _service_web_socket_service__WEBPACK_IMPORTED_MODULE_0__["WebSocketService"](WEBSOCKET_URL, EXAMPLE_URL);
        this.webSocketService.stream().subscribe((message) => {
            this.toastr.info(message.body);
        });
    }
    ngOnInit() {
        if (!this.loginService.isUserLogin()) {
            this.router.navigate(['login']);
        }
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_login_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 3, vars: 0, consts: [[1, "banner-home"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Welcome to GitLab info!");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } }, encapsulation: 2 });


/***/ }),

/***/ "b9wh":
/*!**************************************************************************!*\
  !*** ./src/app/component/nav-bar/merge-request/merge-request.service.ts ***!
  \**************************************************************************/
/*! exports provided: MergeRequestService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MergeRequestService", function() { return MergeRequestService; });
/* harmony import */ var _util_paths__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../util/paths */ "9ZzF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class MergeRequestService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getAll(connectionId) {
        return this.httpClient.get(_util_paths__WEBPACK_IMPORTED_MODULE_0__["MERGE_PATH"] + '/' + connectionId);
    }
}
MergeRequestService.ɵfac = function MergeRequestService_Factory(t) { return new (t || MergeRequestService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
MergeRequestService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: MergeRequestService, factory: MergeRequestService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "fBwR":
/*!******************************************************!*\
  !*** ./src/app/component/nav-bar/chat/chat.model.ts ***!
  \******************************************************/
/*! exports provided: MessageDto */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageDto", function() { return MessageDto; });
class MessageDto {
    constructor() {
        this.id = 0;
        this.senderName = '';
        this.receiverName = '';
        this.messageContent = '';
    }
    deserialize(input) {
        return Object.assign(this, input);
    }
}


/***/ }),

/***/ "g8H4":
/*!****************************************************!*\
  !*** ./src/app/component/login/login.component.ts ***!
  \****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _login_models__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.models */ "Hc/g");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! angularx-social-login */ "ahC7");
/* harmony import */ var _service_web_socket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/web-socket.service */ "1JXD");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login.service */ "6x5B");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");










function LoginComponent_div_9_small_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "small", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Email is required field.");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function LoginComponent_div_9_small_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "small", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Enter valid email.");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function LoginComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, LoginComponent_div_9_small_1_Template, 2, 0, "small", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, LoginComponent_div_9_small_2_Template, 2, 0, "small", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", _r1.errors.required);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", _r1.errors.pattern);
} }
function LoginComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "small", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "Password is required field.");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
const WEBSOCKET_URL = 'ws://localhost:8080/socket';
const EXAMPLE_URL = '/topic/server-broadcaster';
class LoginComponent {
    constructor(loginService, toastr, router, authService) {
        this.loginService = loginService;
        this.toastr = toastr;
        this.router = router;
        this.authService = authService;
        this.user = new _login_models__WEBPACK_IMPORTED_MODULE_0__["UserDto"]();
        this.messageHistory = [];
        this.socialUser = {
            provider: '', id: '', email: '', lastName: '', firstName: '',
            name: '', photoUrl: '', authToken: '', idToken: '', authorizationCode: '', response: ''
        };
        this.userLogged = {};
        this.webSocketService = new _service_web_socket_service__WEBPACK_IMPORTED_MODULE_2__["WebSocketService"](WEBSOCKET_URL, EXAMPLE_URL);
        this.webSocketService.stream().subscribe((message) => {
            this.toastr.info(message.body);
        });
    }
    ngOnInit() {
        if (this.loginService.isUserLogin()) {
            this.loginService.logOut();
        }
    }
    login() {
        this.loginService.login(this.user).subscribe((response) => {
            this.user = new _login_models__WEBPACK_IMPORTED_MODULE_0__["UserDto"]().deserialize(response);
            console.log(response);
            sessionStorage.setItem('user', this.user.email);
            this.router.navigate(['/navbar']);
            this.toastr.success('User logged in successfully.');
        }, (err) => {
            this.toastr.error('Error while login. ' + err.error.message);
        });
    }
    signInWithGoogle() {
        this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_1__["GoogleLoginProvider"].PROVIDER_ID).then(data => {
            this.socialUser = data;
            this.loginGoogle();
        });
    }
    signInWithFB() {
        this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_1__["FacebookLoginProvider"].PROVIDER_ID).then(data => {
            this.socialUser = data;
            this.loginGoogle();
        });
    }
    loginGoogle() {
        this.user.email = this.socialUser.email;
        this.loginService.loginGoogle(this.user.email).subscribe((response) => {
            this.user = new _login_models__WEBPACK_IMPORTED_MODULE_0__["UserDto"]().deserialize(response);
            sessionStorage.setItem('user', this.user.email);
            this.router.navigate(['/navbar']);
        });
    }
}
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](angularx_social_login__WEBPACK_IMPORTED_MODULE_1__["SocialAuthService"])); };
LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: LoginComponent, selectors: [["app-login"]], decls: 21, vars: 9, consts: [[1, "banner"], [1, "form"], [1, "ngform"], ["loginform", "ngForm"], ["type", "email", "placeholder", "Enter email", "name", "email", "required", "", "pattern", "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", 1, "form-control", 3, "ngModel", "ngModelChange"], ["email", "ngModel"], [4, "ngIf"], ["type", "password", "placeholder", "Enter password", "name", "name", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["password", "ngModel"], [1, "btn", "btn-info", "form-control", 3, "disabled", "click"], [1, "btn", "btn-outline-danger", "btn-lg", "btn-block", 3, "click"], [1, "fa", "fa-google"], ["class", "text-danger", 4, "ngIf"], [1, "text-danger"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "Login");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "form", 2, 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "input", 4, 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function LoginComponent_Template_input_ngModelChange_7_listener($event) { return ctx.user.email = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](9, LoginComponent_div_9_Template, 3, 2, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](10, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](11, "input", 7, 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngModelChange", function LoginComponent_Template_input_ngModelChange_11_listener($event) { return ctx.user.password = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](13, LoginComponent_div_13_Template, 3, 0, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function LoginComponent_Template_button_click_15_listener() { return ctx.login(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](16, "Login");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](17, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](18, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function LoginComponent_Template_button_click_18_listener() { return ctx.signInWithGoogle(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](19, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](20, " Login with Google");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](6);
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](8);
        const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵclassProp"]("is-invalid", _r1.invalid && _r1.touched);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngModel", ctx.user.email);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", _r1.errors && (_r1.invalid && _r1.touched));
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵclassProp"]("is-invalid", _r3.invalid && _r3.touched);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngModel", ctx.user.password);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", _r3.invalid && _r3.touched);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("disabled", _r0.form.invalid);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["PatternValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"]], encapsulation: 2 });


/***/ }),

/***/ "ldvO":
/*!********************************************************!*\
  !*** ./src/app/component/nav-bar/nav-bar.component.ts ***!
  \********************************************************/
/*! exports provided: NavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBarComponent", function() { return NavBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../login/login.service */ "6x5B");
/* harmony import */ var _service_connection_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/connection.service */ "njQe");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");






function NavBarComponent_a_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Home");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NavBarComponent_a_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Connections");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NavBarComponent_a_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Applications");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NavBarComponent_a_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Merge request");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NavBarComponent_a_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Commits");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NavBarComponent_a_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Chat");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NavBarComponent_a_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Login");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function NavBarComponent_a_22_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavBarComponent_a_22_Template_a_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.loginService.logOut(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Logout");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
class NavBarComponent {
    constructor(loginService, connections, toastr) {
        this.loginService = loginService;
        this.connections = connections;
        this.toastr = toastr;
    }
    ngOnInit() {
    }
}
NavBarComponent.ɵfac = function NavBarComponent_Factory(t) { return new (t || NavBarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_login_login_service__WEBPACK_IMPORTED_MODULE_1__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_connection_service__WEBPACK_IMPORTED_MODULE_2__["ConnectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"])); };
NavBarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NavBarComponent, selectors: [["app-nav-bar"]], decls: 24, vars: 8, consts: [[1, "header"], [1, "navbar"], [1, "logo"], ["href", "/home"], ["src", "assets/logo..png", "height", "30px", "alt", "GitLab info"], [1, "menu"], ["href", "navbar", "routerLinkActive", "active", 4, "ngIf"], ["href", "navbar/connection", "routerLinkActive", "active", 4, "ngIf"], ["href", "navbar/application", "routerLinkActive", "active", 4, "ngIf"], ["href", "navbar/merge-request", "routerLinkActive", "active", 4, "ngIf"], ["href", "navbar/commit", "routerLinkActive", "active", 4, "ngIf"], ["href", "navbar/chat", "routerLinkActive", "active", 4, "ngIf"], ["href", "/login", "routerLinkActive", "active", 4, "ngIf"], ["href", "/login", "routerLinkActive", "active", 3, "click", 4, "ngIf"], ["href", "navbar", "routerLinkActive", "active"], ["href", "navbar/connection", "routerLinkActive", "active"], ["href", "navbar/application", "routerLinkActive", "active"], ["href", "navbar/merge-request", "routerLinkActive", "active"], ["href", "navbar/commit", "routerLinkActive", "active"], ["href", "navbar/chat", "routerLinkActive", "active"], ["href", "/login", "routerLinkActive", "active"], ["href", "/login", "routerLinkActive", "active", 3, "click"]], template: function NavBarComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "a", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, NavBarComponent_a_8_Template, 2, 0, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, NavBarComponent_a_10_Template, 2, 0, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, NavBarComponent_a_12_Template, 2, 0, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, NavBarComponent_a_14_Template, 2, 0, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, NavBarComponent_a_16_Template, 2, 0, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, NavBarComponent_a_18_Template, 2, 0, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, NavBarComponent_a_20_Template, 2, 0, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, NavBarComponent_a_22_Template, 2, 0, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "router-outlet");
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loginService.isUserLogin());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loginService.isUserLogin());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loginService.isUserLogin());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loginService.isUserLogin());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loginService.isUserLogin());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loginService.isUserLogin());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.loginService.isUserLogin());
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.loginService.isUserLogin());
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterOutlet"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkActive"]], styles: [".header[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    width: 100%;\r\n}\r\n\r\n.header[_ngcontent-%COMP%]   .navbar[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    height: 80px;\r\n    display: flex;\r\n    align-items: center;\r\n    padding-top: 20px;\r\n}\r\n\r\n.header[_ngcontent-%COMP%]   .navbar[_ngcontent-%COMP%]   .logo[_ngcontent-%COMP%] {\r\n    margin-left: 30px;\r\n}\r\n\r\n.header[_ngcontent-%COMP%]   .navbar[_ngcontent-%COMP%]   .logo[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    color: #615a5a;\r\n    font-size: 30px;\r\n    margin-left: 70px;\r\n}\r\n\r\n.header[_ngcontent-%COMP%]   .navbar[_ngcontent-%COMP%]   .menu[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\r\n    margin-left: 192px;\r\n}\r\n\r\n.header[_ngcontent-%COMP%]   .navbar[_ngcontent-%COMP%]   .menu[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    margin-right: 35px;\r\n    align-items: center;\r\n}\r\n\r\n.header[_ngcontent-%COMP%]   .navbar[_ngcontent-%COMP%]   .menu[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    color: #615a5a;\r\n    font-size: 17px;\r\n    font-weight: 500;\r\n    letter-spacing: 1px;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5hdi1iYXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtJQUNsQixXQUFXO0FBQ2Y7O0FBRUE7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksY0FBYztJQUNkLGVBQWU7SUFDZixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtBQUN2QiIsImZpbGUiOiJuYXYtYmFyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaGVhZGVyIC5uYXZiYXIge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDgwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG59XHJcblxyXG4uaGVhZGVyIC5uYXZiYXIgLmxvZ28ge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDMwcHg7XHJcbn1cclxuXHJcbi5oZWFkZXIgLm5hdmJhciAubG9nbyBhIHtcclxuICAgIGNvbG9yOiAjNjE1YTVhO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDcwcHg7XHJcbn1cclxuXHJcbi5oZWFkZXIgLm5hdmJhciAubWVudSB1bCB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTkycHg7XHJcbn1cclxuXHJcbi5oZWFkZXIgLm5hdmJhciAubWVudSB1bCBsaSB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDM1cHg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uaGVhZGVyIC5uYXZiYXIgLm1lbnUgdWwgbGkgYSB7XHJcbiAgICBjb2xvcjogIzYxNWE1YTtcclxuICAgIGZvbnQtc2l6ZTogMTdweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG59XHJcblxyXG4iXX0= */"] });


/***/ }),

/***/ "njQe":
/*!*********************************************************!*\
  !*** ./src/app/component/service/connection.service.ts ***!
  \*********************************************************/
/*! exports provided: ConnectionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionService", function() { return ConnectionService; });
/* harmony import */ var _util_paths__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../util/paths */ "9ZzF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class ConnectionService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    create(connection) {
        return this.httpClient.post(_util_paths__WEBPACK_IMPORTED_MODULE_0__["CONNECTION_PATH"], connection);
    }
    getAll() {
        return this.httpClient.get(_util_paths__WEBPACK_IMPORTED_MODULE_0__["CONNECTION_PATH"]);
    }
    edit(connection) {
        return this.httpClient.put(_util_paths__WEBPACK_IMPORTED_MODULE_0__["CONNECTION_PATH"], connection);
    }
    delete(id) {
        return this.httpClient.delete(_util_paths__WEBPACK_IMPORTED_MODULE_0__["CONNECTION_PATH"] + '/' + id);
    }
}
ConnectionService.ɵfac = function ConnectionService_Factory(t) { return new (t || ConnectionService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
ConnectionService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: ConnectionService, factory: ConnectionService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "oCnx":
/*!****************************************************************************!*\
  !*** ./src/app/component/nav-bar/merge-request/merge-request.component.ts ***!
  \****************************************************************************/
/*! exports provided: MergeRequestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MergeRequestComponent", function() { return MergeRequestComponent; });
/* harmony import */ var _application_application_models__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../application/application.models */ "qNlj");
/* harmony import */ var _connection_connection_models__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../connection/connection.models */ "FPeM");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var src_app_component_service_application_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/component/service/application.service */ "U+dW");
/* harmony import */ var src_app_component_nav_bar_merge_request_merge_request_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/component/nav-bar/merge-request/merge-request.service */ "b9wh");
/* harmony import */ var _login_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../login/login.service */ "6x5B");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-select/ng-select */ "ZOsW");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "3Pt+");











function MergeRequestComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Step 1 - Chose application");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "ng-select", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function MergeRequestComponent_div_4_Template_ng_select_ngModelChange_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r3.application = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, " required ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "button", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function MergeRequestComponent_div_4_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r5.showTwo(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](9, "Next");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("items", ctx_r0.applications)("multiple", false)("searchable", true)("ngModel", ctx_r0.application);
} }
function MergeRequestComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Step 2 - Chose connection");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "ng-select", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function MergeRequestComponent_div_5_Template_ng_select_ngModelChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r6.connection = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, " required ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function MergeRequestComponent_div_5_Template_button_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r8.showOne(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](10, "Back");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "button", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function MergeRequestComponent_div_5_Template_button_click_11_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r7); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r9.showPR(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](12, "Next");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"]("Application: ", ctx_r1.application.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("items", ctx_r1.connections)("multiple", false)("searchable", true)("ngModel", ctx_r1.connection);
} }
function MergeRequestComponent_div_6_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h5", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Merge request ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, "Title: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](10, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](11, "Author: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](14, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](15, "Closed by: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](16, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const merge_r11 = ctx.$implicit;
    const i_r12 = ctx.index;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](i_r12 + 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](merge_r11.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](merge_r11.author);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](merge_r11.closedBy);
} }
function MergeRequestComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Last 10 merge requests");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function MergeRequestComponent_div_6_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r13.showTwo(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](7, "Back");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "button", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function MergeRequestComponent_div_6_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r14); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r15.cancel(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](9, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](10, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](12, MergeRequestComponent_div_6_div_12_Template, 18, 4, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate2"]("Application: ", ctx_r2.application.name, ", Connection: ", ctx_r2.connection.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx_r2.mergeRequests);
} }
class MergeRequestComponent {
    constructor(toastr, applicationService, mergeRequestService, loginService, router) {
        this.toastr = toastr;
        this.applicationService = applicationService;
        this.mergeRequestService = mergeRequestService;
        this.loginService = loginService;
        this.router = router;
        this.application = new _application_application_models__WEBPACK_IMPORTED_MODULE_0__["Application"]();
        this.connection = new _connection_connection_models__WEBPACK_IMPORTED_MODULE_1__["Connection"]();
        this.stepOne = true;
        this.stepTwo = false;
        this.stepPR = false;
        this.mergeRequests = [];
        this.applications = [];
        this.connections = [];
    }
    ngOnInit() {
        if (!this.loginService.isUserLogin()) {
            this.router.navigate(['login']);
        }
        this.applicationService.getAll().subscribe((response) => {
            for (const app of response) {
                this.applications.push(new _application_application_models__WEBPACK_IMPORTED_MODULE_0__["Application"]().deserialize(app));
            }
            this.applications = response;
        });
    }
    showOne() {
        this.stepOne = true;
        this.stepTwo = false;
        this.stepPR = false;
    }
    showTwo() {
        this.stepOne = false;
        this.stepTwo = true;
        this.stepPR = false;
        this.connections = this.application.connections;
    }
    showPR() {
        this.stepOne = false;
        this.stepTwo = false;
        this.stepPR = true;
        this.mergeRequestService.getAll(this.connection.id).subscribe((response) => {
            this.mergeRequests = response;
        }, (err) => {
            this.toastr.error('Can not get any merge request for connection ' + this.connection.name + '.' + err.error.message);
        });
    }
    cancel() {
        this.application = new _application_application_models__WEBPACK_IMPORTED_MODULE_0__["Application"]();
        this.connection = new _connection_connection_models__WEBPACK_IMPORTED_MODULE_1__["Connection"]();
        this.stepOne = true;
        this.stepTwo = false;
        this.stepPR = false;
    }
}
MergeRequestComponent.ɵfac = function MergeRequestComponent_Factory(t) { return new (t || MergeRequestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_component_service_application_service__WEBPACK_IMPORTED_MODULE_4__["ApplicationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_component_nav_bar_merge_request_merge_request_service__WEBPACK_IMPORTED_MODULE_5__["MergeRequestService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_login_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"])); };
MergeRequestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: MergeRequestComponent, selectors: [["app-merge-request"]], decls: 7, vars: 3, consts: [[1, "banner"], [4, "ngIf"], ["placeholder", "Chose application", "bindLabel", "name", "appendTo", "body", "name", "selectedConn", 3, "items", "multiple", "searchable", "ngModel", "ngModelChange"], ["id", "first-button", 1, "btn", "btn-info", 3, "click"], ["placeholder", "Chose connections", "bindLabel", "name", "appendTo", "body", "name", "selectedConn", 3, "items", "multiple", "searchable", "ngModel", "ngModelChange"], [1, "btn", "btn-secondary", 3, "click"], [1, "btn", "btn-info", 3, "click"], [1, "cards"], ["class", "card", 4, "ngFor", "ngForOf"], [1, "card"], [1, "card-title"], [1, "card-body"]], template: function MergeRequestComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Merge requests");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](4, MergeRequestComponent_div_4_Template, 10, 4, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](5, MergeRequestComponent_div_5_Template, 13, 5, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](6, MergeRequestComponent_div_6_Template, 13, 3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.stepOne);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.stepTwo);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.stepPR);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__["NgSelectComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgForOf"]], encapsulation: 2 });


/***/ }),

/***/ "prkM":
/*!********************************************************************************************!*\
  !*** ./src/app/component/nav-bar/application/add-application/add-application.component.ts ***!
  \********************************************************************************************/
/*! exports provided: AddApplicationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddApplicationComponent", function() { return AddApplicationComponent; });
/* harmony import */ var src_app_component_nav_bar_application_application_models__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/component/nav-bar/application/application.models */ "qNlj");
/* harmony import */ var _connection_connection_models__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../connection/connection.models */ "FPeM");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _service_application_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/application.service */ "U+dW");
/* harmony import */ var _service_connection_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/connection.service */ "njQe");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_component_login_login_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/component/login/login.service */ "6x5B");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ng-select/ng-select */ "ZOsW");











function AddApplicationComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "small", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Name is required field.");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function AddApplicationComponent_ng_option_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "ng-option", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const conn_r4 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("value", conn_r4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](conn_r4.name);
} }
const _c0 = function () { return { standalone: true }; };
class AddApplicationComponent {
    constructor(toastr, applicationService, connectionService, router, loginService) {
        this.toastr = toastr;
        this.applicationService = applicationService;
        this.connectionService = connectionService;
        this.router = router;
        this.loginService = loginService;
        this.newApplication = new src_app_component_nav_bar_application_application_models__WEBPACK_IMPORTED_MODULE_0__["Application"]();
        this.selectedItems = [];
        this.connections = [];
    }
    ngOnInit() {
        if (!this.loginService.isUserLogin()) {
            this.router.navigate(['login']);
        }
        this.connectionService.getAll().subscribe((response) => {
            for (const con of response) {
                this.connections.push(new _connection_connection_models__WEBPACK_IMPORTED_MODULE_1__["Connection"]().deserialize(con));
            }
        });
    }
    createApp() {
        this.applicationService.create(this.newApplication).subscribe((response) => {
            this.toastr.success('Application created.');
            this.router.navigate(['navbar/application']);
        }, (err) => {
            this.toastr.error('Creating ' + this.newApplication.name + ' application failed. ' + err.error.message);
        });
    }
    cancelCreateApp() {
        this.router.navigate(['navbar/application']);
        this.newApplication = new src_app_component_nav_bar_application_application_models__WEBPACK_IMPORTED_MODULE_0__["Application"]();
    }
}
AddApplicationComponent.ɵfac = function AddApplicationComponent_Factory(t) { return new (t || AddApplicationComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_service_application_service__WEBPACK_IMPORTED_MODULE_4__["ApplicationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_service_connection_service__WEBPACK_IMPORTED_MODULE_5__["ConnectionService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_component_login_login_service__WEBPACK_IMPORTED_MODULE_7__["LoginService"])); };
AddApplicationComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: AddApplicationComponent, selectors: [["app-add-application"]], decls: 19, vars: 11, consts: [[1, "banner"], [1, "card-add-con"], [1, "inputs"], [1, "ngform"], ["addAppForm", "ngForm"], ["type", "text", "id", "nameNew", "name", "nameNew", "placeholder", "Enter name", "required", "", 1, "form-control", 3, "ngModel", "ngModelChange"], ["nameNew", "ngModel"], [4, "ngIf"], ["bindLabel", "name", "appendTo", "body", 3, "ngModel", "ngModelOptions", "multiple", "searchable", "ngModelChange"], ["name", "name", 3, "value", 4, "ngFor", "ngForOf"], [1, "btn", "btn-info", "form-control", 3, "disabled", "click"], [1, "btn", "btn-secondary", "form-control", 3, "click"], [1, "text-danger"], ["name", "name", 3, "value"]], template: function AddApplicationComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "form", 3, 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6, "Create application");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](7, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "input", 5, 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function AddApplicationComponent_Template_input_ngModelChange_8_listener($event) { return ctx.newApplication.name = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](10, AddApplicationComponent_div_10_Template, 3, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](11, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "ng-select", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngModelChange", function AddApplicationComponent_Template_ng_select_ngModelChange_12_listener($event) { return ctx.newApplication.connections = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](13, AddApplicationComponent_ng_option_13_Template, 2, 2, "ng-option", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](14, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](15, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function AddApplicationComponent_Template_button_click_15_listener() { return ctx.createApp(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](16, "Save");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](17, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function AddApplicationComponent_Template_button_click_17_listener() { return ctx.cancelCreateApp(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](18, "Cancel");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵreference"](4);
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵreference"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵclassProp"]("is-invalid", _r1.touched && _r1.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngModel", ctx.newApplication.name);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", _r1.touched && _r1.invalid);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngModel", ctx.newApplication.connections)("ngModelOptions", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction0"](10, _c0))("multiple", true)("searchable", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx.connections);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("disabled", _r0.form.invalid);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__["NgSelectComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_10__["ɵr"]], styles: ["ng-select.ng-invalid.ng-touched[_ngcontent-%COMP%]   .ng-select-container[_ngcontent-%COMP%] {\r\n    border-color: #dc3545;\r\n    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 0 3px #fde6e8;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1hcHBsaWNhdGlvbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kscUJBQXFCO0lBQ3JCLG1FQUFtRTtBQUN2RSIsImZpbGUiOiJhZGQtYXBwbGljYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIm5nLXNlbGVjdC5uZy1pbnZhbGlkLm5nLXRvdWNoZWQgLm5nLXNlbGVjdC1jb250YWluZXIge1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZGMzNTQ1O1xyXG4gICAgYm94LXNoYWRvdzogaW5zZXQgMCAxcHggMXB4IHJnYmEoMCwgMCwgMCwgMC4wNzUpLCAwIDAgMCAzcHggI2ZkZTZlODtcclxufSJdfQ== */"] });


/***/ }),

/***/ "qNlj":
/*!*********************************************************************!*\
  !*** ./src/app/component/nav-bar/application/application.models.ts ***!
  \*********************************************************************/
/*! exports provided: Application */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Application", function() { return Application; });
class Application {
    constructor() {
        this.name = '';
        this.connections = [];
    }
    deserialize(input) {
        Object.assign(this, input);
        return this;
    }
}


/***/ }),

/***/ "qRRk":
/*!**************************************************************!*\
  !*** ./src/app/component/nav-bar/commit/commit.component.ts ***!
  \**************************************************************/
/*! exports provided: CommitComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommitComponent", function() { return CommitComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var src_app_component_service_application_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/component/service/application.service */ "U+dW");
/* harmony import */ var src_app_component_nav_bar_commit_commit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/component/nav-bar/commit/commit.service */ "rtLy");
/* harmony import */ var src_app_component_login_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/component/login/login.service */ "6x5B");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-select/ng-select */ "ZOsW");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");









function CommitComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Step 1 - Chose application");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ng-select", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CommitComponent_div_4_Template_ng_select_ngModelChange_5_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.application = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, " required ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommitComponent_div_4_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.showTwo(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Next");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx_r0.applications)("multiple", false)("searchable", true)("ngModel", ctx_r0.application);
} }
function CommitComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Step 2 - Chose connection");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ng-select", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CommitComponent_div_5_Template_ng_select_ngModelChange_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.connection = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " required ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommitComponent_div_5_Template_button_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.showOne(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Back");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommitComponent_div_5_Template_button_click_11_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r9.showCommits(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Next");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Application: ", ctx_r1.application.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx_r1.connections)("multiple", false)("searchable", true)("ngModel", ctx_r1.connection);
} }
function CommitComponent_div_6_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h5", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Commit ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Message: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Author: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Sha: ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const commit_r11 = ctx.$implicit;
    const i_r12 = ctx.index;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](i_r12 + 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](commit_r11.message);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](commit_r11.author);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](commit_r11.sha);
} }
function CommitComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Last 10 commits");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommitComponent_div_6_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r13.showTwo(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Back");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommitComponent_div_6_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r14); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r15.cancel(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "hr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, CommitComponent_div_6_div_12_Template, 18, 4, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("Application: ", ctx_r2.application.name, ", Connection: ", ctx_r2.connection.name, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.commits);
} }
class CommitComponent {
    constructor(toastr, applicationService, commitService, loginService, router) {
        this.toastr = toastr;
        this.applicationService = applicationService;
        this.commitService = commitService;
        this.loginService = loginService;
        this.router = router;
        this.applications = [];
        this.connections = [];
        this.stepOne = true;
        this.stepTwo = false;
        this.stepCommits = false;
        this.commits = [];
    }
    ngOnInit() {
        if (!this.loginService.isUserLogin()) {
            this.router.navigate(['login']);
        }
        this.applicationService.getAll().subscribe((response) => {
            this.applications = response;
        });
    }
    showOne() {
        this.connection = '';
        this.stepOne = true;
        this.stepTwo = false;
        this.stepCommits = false;
    }
    showTwo() {
        this.stepOne = false;
        this.stepTwo = true;
        this.stepCommits = false;
        this.connections = this.application.connections;
    }
    showCommits() {
        this.stepOne = false;
        this.stepTwo = false;
        this.stepCommits = true;
        this.commitService.getAll(this.connection.id).subscribe((response) => {
            this.commits = response;
        }, (err) => {
            this.toastr.error('Can not get any commit for connection ' + this.connection.name + '. ' + err.error.message);
        });
    }
    cancel() {
        this.application = '';
        this.connection = '';
        this.stepOne = true;
        this.stepTwo = false;
        this.stepCommits = false;
    }
}
CommitComponent.ɵfac = function CommitComponent_Factory(t) { return new (t || CommitComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_1__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_component_service_application_service__WEBPACK_IMPORTED_MODULE_2__["ApplicationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_component_nav_bar_commit_commit_service__WEBPACK_IMPORTED_MODULE_3__["CommitService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_component_login_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"])); };
CommitComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CommitComponent, selectors: [["app-commit"]], decls: 7, vars: 3, consts: [[1, "banner"], [4, "ngIf"], ["placeholder", "Chose application", "bindLabel", "name", "appendTo", "body", "name", "selectedConn", 3, "items", "multiple", "searchable", "ngModel", "ngModelChange"], ["id", "first-button", 1, "btn", "btn-info", 3, "click"], ["placeholder", "Chose connections", "bindLabel", "name", "appendTo", "body", "name", "selectedConn", 3, "items", "multiple", "searchable", "ngModel", "ngModelChange"], [1, "btn", "btn-secondary", 3, "click"], [1, "btn", "btn-info", 3, "click"], [1, "cards"], ["class", "card", 4, "ngFor", "ngForOf"], [1, "card"], [1, "card-title"], [1, "card-body"]], template: function CommitComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Commits");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, CommitComponent_div_4_Template, 10, 4, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, CommitComponent_div_5_Template, 13, 5, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, CommitComponent_div_6_Template, 13, 3, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.stepOne);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.stepTwo);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.stepCommits);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_7__["NgSelectComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"]], encapsulation: 2 });


/***/ }),

/***/ "rtLy":
/*!************************************************************!*\
  !*** ./src/app/component/nav-bar/commit/commit.service.ts ***!
  \************************************************************/
/*! exports provided: CommitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommitService", function() { return CommitService; });
/* harmony import */ var _util_paths__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../util/paths */ "9ZzF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class CommitService {
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    getAll(connectionId) {
        return this.httpClient.get(_util_paths__WEBPACK_IMPORTED_MODULE_0__["COMMIT_PATH"] + '/' + connectionId);
    }
}
CommitService.ɵfac = function CommitService_Factory(t) { return new (t || CommitService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
CommitService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: CommitService, factory: CommitService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _component_login_login_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./component/login/login.component */ "g8H4");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");




const routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: _component_login_login_component__WEBPACK_IMPORTED_MODULE_1__["LoginComponent"] },
    { path: 'navbar', loadChildren: () => __webpack_require__.e(/*! import() | component-nav-bar-navbar-module */ "component-nav-bar-navbar-module").then(__webpack_require__.bind(null, /*! ./component/nav-bar/navbar.module */ "r28+")).then(mod => mod.NavbarModule) }
];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map