--Connections
INSERT INTO connection (branch_name, name, project, url, projectID) VALUES ('master', 'Klinika - master', 'Klinika', 'https://gitlab.com/helena_anisic/klinika', '17562199')
INSERT INTO connection (branch_name, name, project, url, projectID) VALUES ('patient', 'Klinika - patient', 'Klinika', 'https://gitlab.com/helena_anisic/klinika', '17562199')
INSERT INTO connection (branch_name, name, project, url, projectID) VALUES ('master', 'CareAndCure-Hospital - master', 'CareAndCure-Hospital', 'https://gitlab.com/marijamilanovic/projekat1-klinika', '17558544')
INSERT INTO connection (branch_name, name, project, url, projectID) VALUES ('develop', 'CareAndCure-Hospital - develop', 'CareAndCure-Hospital', 'https://gitlab.com/marijamilanovic/projekat1-klinika', '17558544')
INSERT INTO connection (branch_name, name, project, url, projectID) VALUES ('master', 'QaCube - master', 'QaCube', 'https://gitlab.com/majastamenic/qacube', '26420199')
INSERT INTO connection (branch_name, name, project, url, projectID) VALUES ('qacube-gitlab-info-majastamenic', 'QaCube ', 'QaCube ', 'https://gitlab.com/majastamenic/qacube', '26420199')


--Applications
INSERT INTO application (`name`) VALUES ('Klinika');
INSERT INTO application (`name`) VALUES ('CareAndCure-Hospital');

--Link table
INSERT INTO application_connections (application_id, connections_id) VALUES (1, 1);
INSERT INTO application_connections (application_id, connections_id) VALUES (1, 2);
INSERT INTO application_connections (application_id, connections_id) VALUES (2, 3);
INSERT INTO application_connections (application_id, connections_id) VALUES (2, 4);

--Users
INSERT INTO my_users (email, password) VALUES ('masa@mailinator.com', 'masa123!');
INSERT INTO my_users (email, password) VALUES ('teodora@mailinator.com', 'teodora123!');
INSERT INTO my_users (email, password) VALUES ('bojan@mailinator.com', 'bojan123!');
INSERT INTO my_users (email, password) VALUES ('ivona@mailinator.com', 'ivona123!');
INSERT INTO my_users (email, password) VALUES ('milica@mailinator.com', 'milica123!');

--Chat
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Zdravo', 'teodora@mailinator.com', 'masa@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Cao', 'masa@mailinator.com', 'teodora@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Kako si?', 'bojan@mailinator.com', 'ivona@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Sta radis?', 'ivona@mailinator.com', 'bojan@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Sta ima novo?', 'milica@mailinator.com', 'masa@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Cujemo se', 'teodora@mailinator.com', 'milica@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Cao', 'bojan@mailinator.com', 'masa@mailinator.com');

INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Kako si?', 'masa@mailinator.com', 'ivona@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Sta radis?', 'masa@mailinator.com', 'bojan@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Dobar dan', 'masa@mailinator.com', 'masa@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Cujemo se', 'masa@mailinator.com', 'milica@mailinator.com');
INSERT INTO messages_records (message_content, receiver_name, sender_name) VALUES ('Cao', 'masa@mailinator.com', 'masa@mailinator.com');