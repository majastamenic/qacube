package Backend.domain;

import lombok.Data;

import java.util.Date;


@Data
public class Commit {

    private String author;
    private String sha;
    private String message;
    private Date created;

}
