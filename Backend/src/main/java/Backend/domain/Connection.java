package Backend.domain;

import Backend.validation.Url;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@Entity
public class Connection implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    @NotBlank(message = "Connection name can't be blank.")
    private String name;
    @Url
    @Column
    private String url;
    @Column
    private String token;
    @Column
    @NotBlank(message = "Project name can't be blank.")
    private String project;
    @Column
    @NotBlank(message = "Branch name can't be blank.")
    private String branchName;
    @Column
    @NotBlank(message = "Project ID can't be blank.")
    private String projectID;
}
