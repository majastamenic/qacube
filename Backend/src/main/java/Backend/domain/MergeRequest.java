package Backend.domain;

import lombok.Data;

import java.util.Date;

@Data
public class MergeRequest {

    private String author;
    private String closedBy;
    private String title;
    private Date closedAt;
}
