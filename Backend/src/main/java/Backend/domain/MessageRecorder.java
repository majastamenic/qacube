package Backend.domain;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="messagesRecords")
public class MessageRecorder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    String senderName;

    @NotNull
    String receiverName;

    @NotNull
    String messageContent;
}
