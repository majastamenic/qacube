package Backend.repository;

import Backend.domain.Application;
import Backend.domain.Connection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {

    Application findApplicationById(Long id);

    Application findApplicationByConnections(Connection connection);

    Application findApplicationByName(String name);
}
