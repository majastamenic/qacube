package Backend.repository;

import Backend.domain.Connection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long> {
    Connection findConnectionById(Long id);
    void deleteById(Long id);
    Connection findConnectionByName(String name);
}
