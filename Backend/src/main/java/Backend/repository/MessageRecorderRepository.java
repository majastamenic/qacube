package Backend.repository;

import Backend.domain.MessageRecorder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRecorderRepository extends JpaRepository<MessageRecorder, Long> {
    List<MessageRecorder> findAllByReceiverName(String receiverName);
}
