package Backend.controller;

import Backend.domain.Commit;
import Backend.service.CommitService;
import Backend.service.interfaces.ICommitService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/commit")
@CrossOrigin(origins = "http://localhost:4200")
public class CommitController {

    private ICommitService commitService;

    public CommitController(CommitService commitService) {
        this.commitService = commitService;
    }

    @GetMapping("/{connectionId}")
    public List<Commit> getCommits(@PathVariable Long connectionId) {
        log.info("Try to get commits for connection with id:" + connectionId + ".");
        return commitService.getCommits(connectionId);
    }
}
