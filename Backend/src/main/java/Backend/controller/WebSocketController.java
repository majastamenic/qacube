package Backend.controller;

import Backend.domain.Commit;
import Backend.domain.Connection;
import Backend.service.ConnectionService;
import Backend.service.GitLabService;
import Backend.service.interfaces.IConnectionService;
import Backend.service.interfaces.IGitlabService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.util.Date;
import java.util.List;

@Controller
public class WebSocketController {

    private static final String SENDING_URL = "/topic/server-broadcaster";

    private IConnectionService connectionService;
    private IGitlabService gitlabService;
    private final SimpMessagingTemplate template;


    @Autowired
    public WebSocketController(SimpMessagingTemplate template, ConnectionService connectionService, GitLabService gitlabService) {
        this.template = template;
        this.connectionService = connectionService;
        this.gitlabService = gitlabService;
    }

    @SubscribeMapping(SENDING_URL)
    public String onSubscribe() {
        return "Connected to backed.";
    }

    @Scheduled(fixedRate = 5000)
    public void sendMessage() throws Exception {

        Date now = new Date();
        List<Commit> commits;

        for(Connection connection: connectionService.getAllConnections()){
            commits = gitlabService.getCommits(connection);

            for(Commit commit: commits){
                if(commit.getCreated().getTime() < now.getTime() && commit.getCreated().getTime() > now.getTime()-5000 ) {
                    template.convertAndSend(SENDING_URL, "New commit.");
                    break;
                }
            }
            break;
        }
    }
}