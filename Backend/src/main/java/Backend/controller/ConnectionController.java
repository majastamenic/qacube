package Backend.controller;

import Backend.domain.Connection;
import Backend.controller.dto.ConnectionDto;
import Backend.controller.mapping.ConnectionMapper;
import Backend.service.ConnectionService;
import Backend.service.interfaces.IConnectionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/connection")
@CrossOrigin(origins = "http://localhost:4200")
public class ConnectionController {

    private IConnectionService connectionService;

    public ConnectionController(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @GetMapping
    public List<Connection> getAll() {
        log.info("Try to get all connections.");
        return connectionService.getAllConnections();
    }

    @PostMapping
    public Connection create(@RequestBody ConnectionDto connectionDto) throws Exception {
        log.info("Try to create connection: " + connectionDto.getName() + ", url: " + connectionDto.getUrl() + ".");
        return connectionService.createConnection(ConnectionMapper.mapConnectionDtoToConnection(connectionDto));
    }

    @PutMapping
    public Connection edit(@RequestBody Connection connection) {
        log.info("Try to edit connection: " + connection.getId() + ".");
        return connectionService.editConnection(connection);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        log.info("Try to delete connection with id: " + id + ".");
        connectionService.deleteConnection(id);
    }
}
