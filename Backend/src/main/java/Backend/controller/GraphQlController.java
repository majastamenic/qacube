package Backend.controller;

import Backend.service.GraphQlService;
import graphql.ExecutionResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/graphql")
@CrossOrigin(origins = "http://localhost:4200")
public class GraphQlController {

    @Autowired
    private GraphQlService graphQLService;

    @PostMapping
    public ResponseEntity<Object> findAllGraphQL(@RequestBody String query){
        ExecutionResult execute = graphQLService.getGraphQL().execute(query);
        return new ResponseEntity<>(execute, HttpStatus.OK);
    }
}
