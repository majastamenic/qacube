package Backend.controller;

import Backend.domain.User;
import Backend.controller.dto.UserDto;
import Backend.controller.mapping.UserMapping;
import Backend.service.UserService;
import Backend.service.interfaces.IUserService;
import graphql.ExecutionResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    private IUserService userService;

    public UserController(UserService userService) {

        this.userService = userService;
    }

    @PostMapping
    public User login(@RequestBody UserDto userDto) {
        log.info("Try to login user: " + userDto.getEmail() + ".");
        return userService.login(UserMapping.mapUserDtoToUser(userDto));
    }

    @GetMapping("/{email}")
    public  User loginGoogle(@PathVariable String email){
        log.info("Try to login user "+ email +" - google or facebook");
        return userService.loginGoogle(email);
    }

    @GetMapping
    public List<User> getAll(){
        return userService.findAll();
    }

}
