package Backend.controller;

import Backend.domain.MergeRequest;
import Backend.service.MergeRequestService;
import Backend.service.interfaces.IMergeRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/merge-request")
@CrossOrigin(origins = "http://localhost:4200")
public class MergeRequestController {

    private IMergeRequestService mergeRequestService;

    public MergeRequestController(MergeRequestService mergeRequestService) {
        this.mergeRequestService = mergeRequestService;
    }

    @GetMapping("/{connectionId}")
    public List<MergeRequest> getMergeRequests(@PathVariable Long connectionId) throws Exception {
        log.info("Try to get merge requests for connection with id: " + connectionId + ".");
        return mergeRequestService.getMergeRequests(connectionId);
    }
}
