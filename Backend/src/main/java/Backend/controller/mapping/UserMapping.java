package Backend.controller.mapping;

import Backend.domain.User;
import Backend.controller.dto.UserDto;

public class UserMapping {

    public static User mapUserDtoToUser(UserDto userDto){
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        return user;
    }

}
