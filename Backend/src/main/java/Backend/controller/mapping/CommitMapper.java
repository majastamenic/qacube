package Backend.controller.mapping;

import Backend.domain.Commit;
import Backend.controller.dto.CommitDto;

import java.util.ArrayList;
import java.util.List;

public class CommitMapper {

    private static Commit mapCommitDtoToCommit(CommitDto commitDto) {
        Commit commit = new Commit();

        commit.setSha(commitDto.getId());
        commit.setAuthor(commitDto.getAuthor_name());
        commit.setMessage(commitDto.getMessage());
        commit.setCreated(commitDto.getCreated_at());

        return commit;
    }

    public static List<Commit> mapCommitsDtoToCommits(List<CommitDto> commitDtos) {
        List<Commit> commits = new ArrayList<>();

        for (CommitDto commitDto : commitDtos)
            commits.add(mapCommitDtoToCommit(commitDto));

        return commits;
    }
}
