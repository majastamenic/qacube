package Backend.controller.mapping;

import Backend.domain.Connection;
import Backend.controller.dto.ConnectionDto;

public class ConnectionMapper {

    public static Connection mapConnectionDtoToConnection(ConnectionDto connectionDto) {
        Connection connection = new Connection();
        connection.setName(connectionDto.getName());
        connection.setToken(connectionDto.getToken());
        connection.setUrl(connectionDto.getUrl());
        connection.setProject(connectionDto.getProject());
        connection.setBranchName(connectionDto.getBranchName());

        return connection;
    }
}
