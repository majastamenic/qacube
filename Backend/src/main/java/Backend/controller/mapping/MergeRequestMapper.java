package Backend.controller.mapping;

import Backend.domain.MergeRequest;
import Backend.controller.dto.MergeRequests.MergeRequestDto;

import java.util.ArrayList;
import java.util.List;

public class MergeRequestMapper {

    private static MergeRequest mapMergeRequestDtoToMergeRequest(MergeRequestDto mergeRequestDto) {
        MergeRequest mergeRequest = new MergeRequest();
        mergeRequest.setTitle(mergeRequestDto.getTitle());
        mergeRequest.setAuthor(mergeRequestDto.getAuthor().getName());
        mergeRequest.setClosedBy(mergeRequestDto.getClosed_by().getName());
        mergeRequest.setClosedAt(mergeRequestDto.getClosed_at());

        return mergeRequest;
    }

    public static List<MergeRequest> mapMergeRequestsDtoToMergeRequests(List<MergeRequestDto> mergeRequestDtoList) {
        List<MergeRequest> mergeRequests = new ArrayList<>();

        for (MergeRequestDto mergeRequestDto : mergeRequestDtoList)
            mergeRequests.add(mapMergeRequestDtoToMergeRequest(mergeRequestDto));

        return mergeRequests;
    }
}
