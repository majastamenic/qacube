package Backend.controller.mapping;

import Backend.domain.Application;
import Backend.controller.dto.ApplicationDto;

public class ApplicationMapper {

    public static Application mapApplicationDtoToApplication(ApplicationDto applicationDto) {
        Application application = new Application();
        application.setName(applicationDto.getName());
        application.setConnections(applicationDto.getConnections());
        return application;
    }
}
