package Backend.controller;

import Backend.domain.MessageRecorder;
import Backend.domain.User;
import Backend.service.ChatService;
import Backend.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ChatController {

    @Autowired
    private UserService userService;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private ChatService chatService;


    @GetMapping("api/mymessages")
    public List<MessageRecorder> getMyMessage(@RequestParam String receiverName) throws Exception{
        User user = userService.findByEmail(receiverName);
        if(user.getId() == null) {
            throw new Exception("There is no user with this " + user.getEmail() + "user name!");
        }
        List<MessageRecorder> response = chatService.findAllByReceiverName(receiverName);
        return response;

    }

    @PostMapping("api/chat")
    public void send(@RequestBody MessageRecorder messageRecorder){
        chatService.save(messageRecorder);
    }




    /*@Scheduled(fixedRate = 1000)
    public void getMyMessages() throws Exception {
        User user = userService.findByEmail(UserService.loginUser.getEmail());
        if(user.getId() == null) {
            throw new Exception("There is no user with this " + user.getEmail() + "user name!");
        }
        List<MessageRecorder> response = chatService.findAllByReceiverName(UserService.loginUser.getEmail());
        simpMessagingTemplate.convertAndSend(SENDING_URL, response);
    }*/

}
