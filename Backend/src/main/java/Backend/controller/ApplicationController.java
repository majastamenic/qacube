package Backend.controller;

import Backend.controller.dto.ApplicationDto;
import Backend.controller.mapping.ApplicationMapper;
import Backend.domain.Application;
import Backend.service.ApplicationService;
import Backend.service.interfaces.IApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/application")
@CrossOrigin(origins = "http://localhost:4200")
public class ApplicationController {

    private IApplicationService applicationService;

    public ApplicationController(ApplicationService applicationService) {
        this.applicationService = applicationService;
    }

    @GetMapping
    public List<Application> getAll() {
        log.info("Try to get all applications.");
        return applicationService.getAllApplications();
    }

    @PostMapping
    public Application create(@RequestBody ApplicationDto applicationDto) {
        log.info("Try to create application: " + applicationDto.getName() + ".");
        return applicationService.createApplication(ApplicationMapper.mapApplicationDtoToApplication(applicationDto));
    }

    @PutMapping
    public Application edit(@RequestBody Application application) {
        log.info("Try to edit application: " + application.getId() + " " + application.getName() + ".");
        return applicationService.editApplication(application);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        log.info("Try to delete application with id: " + id + ".");
        applicationService.delete(id);
    }

}
