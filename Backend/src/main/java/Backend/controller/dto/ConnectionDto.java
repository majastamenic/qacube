package Backend.controller.dto;

import Backend.validation.Url;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ConnectionDto {

    @NotBlank(message = "Connection name can't be blank.")
    private String name;
    @Url
    private String url;
    private String token;
    @NotBlank(message = "Project name can't be blank.")
    private String project;
    @NotBlank(message = "Branch name can't be blank.")
    private String branchName;
}
