package Backend.controller.dto;

import Backend.domain.Connection;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ApplicationDto {
    @NotBlank(message = "Application name can't be blank.")
    private String name;
    private List<Connection> connections;
}
