package Backend.controller.dto.MergeRequests;

import lombok.Data;

import java.util.Date;

@Data
public class MergeRequestDto {
    private String title;
    private ClosedByDto closed_by;
    private AuthorDto author;
    private Date closed_at;
}
