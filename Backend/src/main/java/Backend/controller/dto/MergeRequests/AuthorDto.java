package Backend.controller.dto.MergeRequests;

import lombok.Data;

@Data
public class AuthorDto {
    private String name;
}
