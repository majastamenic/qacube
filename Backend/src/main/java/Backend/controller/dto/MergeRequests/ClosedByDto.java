package Backend.controller.dto.MergeRequests;

import lombok.Data;

@Data
public class ClosedByDto {
    private String name;
}
