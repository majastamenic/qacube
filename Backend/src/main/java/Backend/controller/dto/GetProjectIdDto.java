package Backend.controller.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class GetProjectIdDto implements Serializable {
    private String id;
    private String name;
    private String web_url;
}
