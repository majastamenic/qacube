package Backend.controller.dto;

import lombok.Data;

import java.util.Date;

@Data
public class CommitDto {
    private String author_name;
    private String message;
    private String id;
    private Date created_at;
}
