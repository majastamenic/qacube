package Backend.service;

import Backend.domain.Commit;
import Backend.domain.Connection;
import Backend.service.interfaces.ICommitService;
import Backend.service.interfaces.IConnectionService;
import Backend.service.interfaces.IGitlabService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CommitService implements ICommitService {

    private IGitlabService gitlabService;
    private IConnectionService connectionService;

    public CommitService(GitLabService gitLabService, ConnectionService connectionService) {
        this.gitlabService = gitLabService;
        this.connectionService = connectionService;
    }

    public List<Commit> getCommits(Long connectionId) {
        Connection connection = connectionService.findConnectionById(connectionId);
        return gitlabService.getCommits(connection);
    }
}
