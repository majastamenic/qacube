package Backend.service;

import Backend.domain.Application;
import Backend.domain.Connection;
import Backend.repository.ApplicationRepository;
import Backend.service.exception.BadRequestException;
import Backend.service.exception.NotFoundException;
import Backend.service.interfaces.IApplicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ApplicationService implements IApplicationService {

    private ApplicationRepository applicationRepository;

    public ApplicationService(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    public Application createApplication(Application application) {
        Application dbApplication = new Application();
        try {
            dbApplication = applicationRepository.save(application);
        } catch (Exception e) {
            log.error("Failed to store application: " + application.getName());
            throw new BadRequestException("Can't save application with name: " + application.getName());
        }
        return dbApplication;
    }

    private Application findApplicationById(Long id) {
        Application application = applicationRepository.findApplicationById(id);
        if (application == null) {
            log.info("Application with id:" + id + "doesn't exists.");
            throw new NotFoundException("There is no app with id: " + id);
        }
        return application;
    }

    public List<Application> getAllApplications() {
        List<Application> applications = applicationRepository.findAll();
        if (applications.isEmpty()) {
            log.info("Applictions is empty");
            throw new NotFoundException("There is no any application");
        }
        return applications;
    }

    public Application editApplication(Application application) {
        Application dbAppication = findApplicationById(application.getId());
        dbAppication.setName(application.getName());
        dbAppication.setConnections(application.getConnections());
        log.info("Try to save modified application.");
        applicationRepository.save(dbAppication);
        return dbAppication;
    }

    public void delete(Long id) {
        Application application = findApplicationById(id);
        try {
            applicationRepository.delete(application);
        } catch (Exception e) {
            log.error("Failed to delete application with id: " + id);
            throw new BadRequestException("Can't delete application with id: " + id);
        }
    }

    public void deleteConnection(Connection connection) {
        Application application = applicationRepository.findApplicationByConnections(connection);
        application.getConnections().remove(connection);
        applicationRepository.save(application);
    }

    public List<Application> getApplications() {
        return applicationRepository.findAll();
    }
}
