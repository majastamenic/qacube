package Backend.service;

import Backend.domain.MessageRecorder;
import Backend.repository.MessageRecorderRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatService {

    private MessageRecorderRepository messageRecorderRepository;

    public ChatService(MessageRecorderRepository messageRecorderRepository){
        this.messageRecorderRepository = messageRecorderRepository;
    }

    public List<MessageRecorder> findAllByReceiverName(String receiverName) {
        return messageRecorderRepository.findAllByReceiverName(receiverName);
    }

    public void save(MessageRecorder messageRecorder) {
        messageRecorderRepository.save(messageRecorder);
    }
}
