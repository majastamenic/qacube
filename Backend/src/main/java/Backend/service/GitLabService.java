package Backend.service;

import Backend.domain.Commit;
import Backend.domain.Connection;
import Backend.domain.MergeRequest;
import Backend.controller.dto.CommitDto;
import Backend.controller.dto.GetProjectIdDto;
import Backend.controller.dto.MergeRequests.MergeRequestDto;
import Backend.service.exception.BadRequestException;
import Backend.controller.mapping.CommitMapper;
import Backend.controller.mapping.MergeRequestMapper;
import Backend.service.interfaces.IGitlabService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class GitLabService implements IGitlabService {

    @Autowired
    private RestTemplate restTemplate;

    public Connection getProjectID(Connection connection) {
        String url = getUrl(connection.getUrl()) + "/projects?search=" + connection.getProject();
        GetProjectIdDto getProjectIdDto = new GetProjectIdDto();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("PRIVATE-TOKEN", connection.getToken());
            HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
            ResponseEntity<GetProjectIdDto[]> getProjectIdDtos = restTemplate.exchange(url, HttpMethod.GET, entity, GetProjectIdDto[].class, 1);
            List<GetProjectIdDto> getProjectIdDtoList = Arrays.asList(getProjectIdDtos.getBody());

            //Project name is unique, but GitLab api has a "contains" search
            for (GetProjectIdDto projectIdDto : getProjectIdDtoList) {
                if (projectIdDto.getName().equals(connection.getProject()) && projectIdDto.getWeb_url().equals(connection.getUrl())) {
                    getProjectIdDto = projectIdDto;
                    break;
                }
            }

        } catch (Exception e) {
            log.info("Can't communication with gitlab");
            throw new BadRequestException("Can't communication with GitLab API.");
        }
        log.info("Set projectId to connection: " + connection.getName());
        connection.setProjectID(getProjectIdDto.getId());

        return connection;
    }

    public List<MergeRequest> getMergeRequests(Connection connection) {

        String url = getUrl(connection.getUrl()) + "/projects/" + connection.getProjectID() + "/merge_requests?target_branch=" + connection.getBranchName() + "&order_by=updated_at";
        List<MergeRequest> mergeRequests;

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("PRIVATE-TOKEN", connection.getToken());
            HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
            ResponseEntity<MergeRequestDto[]> mergeRequestDtos = restTemplate.exchange(url, HttpMethod.GET, entity, MergeRequestDto[].class, 1);
            List<MergeRequestDto> mergeRequestList = Arrays.asList(mergeRequestDtos.getBody());

            //We need only last 10 merge requests

            try{
                mergeRequestList = mergeRequestList.stream().sorted(Comparator.comparing(MergeRequestDto::getClosed_at).reversed()).limit(10).collect(Collectors.toList());
            }catch (Exception e){
                mergeRequestList = mergeRequestList.stream().limit(10).collect(Collectors.toList());
            }
            mergeRequests = MergeRequestMapper.mapMergeRequestsDtoToMergeRequests(mergeRequestList);

        } catch (Exception e) {
            e.printStackTrace();
            log.info("Communication with GitLab failed.");
            throw new BadRequestException("Communication with GitLab failed. " + e.getMessage());
        }

        return mergeRequests;
    }

    public List<Commit> getCommits(Connection connection) {
        String url = getUrl(connection.getUrl()) + "/projects/" + connection.getProjectID() + "/repository/commits?branch=" + connection.getBranchName();
        List<Commit> commits;

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("PRIVATE-TOKEN", connection.getToken());
            HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
            ResponseEntity<CommitDto[]> commitDtos = restTemplate.exchange(url, HttpMethod.GET, entity, CommitDto[].class, 1);
            List<CommitDto> commitDtoList = Arrays.asList(commitDtos.getBody());

            //We need only last 10 commits
            commitDtoList = commitDtoList.stream().sorted(Comparator.comparing(CommitDto::getCreated_at).reversed()).limit(10).collect(Collectors.toList());
            commits = CommitMapper.mapCommitsDtoToCommits(commitDtoList);

        } catch (Exception e) {
            e.printStackTrace();
            log.info("Communication with GitLab failed");
            throw new BadRequestException("Communication with GitLab failed. " + e.getMessage());
        }
        return commits;
    }

    private String getUrl(String connectionUrl){
        return  "https://" + connectionUrl.split("/")[2] + "/api/v4";
    }


}
