package Backend.service;

import Backend.domain.User;
import Backend.repository.UserRepository;
import Backend.service.exception.UnauthorizeException;
import Backend.service.interfaces.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserService implements IUserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User login(User user) {
        User dbUser = userRepository.findUserByEmailAndPassword(user.getEmail(), user.getPassword());
        if(dbUser ==null){
            log.info("There is no user with email: " + user.getEmail());
            throw new UnauthorizeException("Can't find user with email: " + user.getEmail());
        }
        return dbUser;
    }

    public User loginGoogle(String email) {
        User dbUser = userRepository.findUserByEmail(email);
        if(dbUser == null){
            log.info("Try to save user: " + email + " -login with google or facebook");
            User user = new User();
            user.setEmail(email);
            return userRepository.save(user);
        }
        return login(dbUser);
    }

    public User findByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
