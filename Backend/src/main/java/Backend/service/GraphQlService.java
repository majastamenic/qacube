package Backend.service;

import Backend.service.datafetcher.ApplicationFetcher;
import Backend.service.datafetcher.ApplicationsFetcher;
import Backend.service.datafetcher.ConnectionFetcher;
import Backend.service.datafetcher.ConnectionsFetcher;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;

@Service
public class GraphQlService {

    private GraphQL graphQL;
    @Autowired
    private ApplicationFetcher applicationFetcher;
    @Autowired
    private ApplicationsFetcher applicationsFetcher;
    @Autowired
    private ConnectionFetcher connectionFetcher;
    @Autowired
    private ConnectionsFetcher connectionsFetcher;

    @PostConstruct
    public void loadSchema() throws IOException {
        URL url = Resources.getResource("gitlab.graphql");
        String sdl = Resources.toString(url, Charsets.UTF_8);
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(sdl);
        RuntimeWiring wiring = buildRuntimeWiring();
        GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    private RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type("Query", typeWiring -> typeWiring
                        .dataFetcher("applications", applicationsFetcher)
                        .dataFetcher("application", applicationFetcher)
                        .dataFetcher("connection", connectionFetcher)
                        .dataFetcher("connections", connectionsFetcher))
                .build();
    }


    public GraphQL getGraphQL() {
        return graphQL;
    }

}
