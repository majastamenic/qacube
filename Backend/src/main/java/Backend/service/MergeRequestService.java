package Backend.service;

import Backend.domain.Connection;
import Backend.domain.MergeRequest;
import Backend.service.interfaces.IConnectionService;
import Backend.service.interfaces.IGitlabService;
import Backend.service.interfaces.IMergeRequestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class MergeRequestService implements IMergeRequestService {

    private IGitlabService gitlabService;
    private IConnectionService connectionService;

    public MergeRequestService(GitLabService gitLabService, ConnectionService connectionService) {
        this.gitlabService = gitLabService;
        this.connectionService = connectionService;
    }

    public List<MergeRequest> getMergeRequests(Long connectionId) throws Exception {
        Connection connection = connectionService.findConnectionById(connectionId);
        return gitlabService.getMergeRequests(connection);
    }
}
