package Backend.service;

import Backend.domain.Connection;
import Backend.service.exception.BadRequestException;
import Backend.service.exception.NotFoundException;
import Backend.repository.ConnectionRepository;
import Backend.service.interfaces.IApplicationService;
import Backend.service.interfaces.IConnectionService;
import Backend.service.interfaces.IGitlabService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ConnectionService implements IConnectionService {

    private ConnectionRepository connectionRepository;
    private IApplicationService applicationService;
    private IGitlabService gitlabService;

    public ConnectionService(ConnectionRepository connectionRepository,
                             ApplicationService applicationService, GitLabService gitLabService) {
        this.connectionRepository = connectionRepository;
        this.applicationService = applicationService;
        this.gitlabService = gitLabService;
    }

    public Connection createConnection(Connection connection) throws Exception {
        Connection dbConnection;
        connection = gitlabService.getProjectID(connection);
        try {
            dbConnection = connectionRepository.save(connection);
        } catch (Exception e) {
            log.error("Failed to store connection: " + connection.getName());
            throw new BadRequestException("Can't save connection with name: " + connection.getName());
        }
        return dbConnection;
    }

    public Connection findConnectionById(Long id) {
        Connection connection = connectionRepository.findConnectionById(id);
        if (connection == null) {
            log.info("Connection with id:" + id + "doesn't exists.");
            throw new NotFoundException("There is no connection with id: " + id);
        }
        return connection;
    }

    public List<Connection> getAllConnections() {
        List<Connection> connections = connectionRepository.findAll();
        if (connections.isEmpty()) {
            log.info("Connections is empty.");
            throw new NotFoundException("There is no connection.");
        }
        return connections;
    }

    public Connection editConnection(Connection newConnection) {
        Connection connection = findConnectionById(newConnection.getId());
        connection.setName(newConnection.getName());
        connection.setBranchName(newConnection.getBranchName());
        connection.setProject(newConnection.getProject());
        connection.setToken(newConnection.getToken());
        connection.setUrl(newConnection.getUrl());
        log.info("Try to save modified connection.");
        connectionRepository.save(connection);
        return connection;
    }

    public void deleteConnection(Long id) {
        Connection dbConnection = findConnectionById(id);
        try {
            applicationService.deleteConnection(dbConnection);
            connectionRepository.delete(dbConnection);
        } catch (Exception e) {
            log.error("Failed to delete connection with id: " + id);
            throw new BadRequestException("Can't delete connection with id: " + id);
        }
    }
}
