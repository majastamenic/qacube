package Backend.service.datafetcher;

import Backend.domain.Application;
import Backend.repository.ApplicationRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApplicationFetcher implements DataFetcher<Application> {

    @Autowired
    private ApplicationRepository applicationRepository;

    public Application get(DataFetchingEnvironment dataFetchingEnvironment) {

        String name = dataFetchingEnvironment.getArgument("name");

        return applicationRepository.findApplicationByName(name);
    }
}
