package Backend.service.datafetcher;

import Backend.domain.Application;
import Backend.repository.ApplicationRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApplicationsFetcher implements DataFetcher<List<Application>> {
    @Autowired
    private ApplicationRepository applicationRepository;

    public List<Application> get(DataFetchingEnvironment dataFetchingEnvironment) {
        return applicationRepository.findAll();
    }
}
