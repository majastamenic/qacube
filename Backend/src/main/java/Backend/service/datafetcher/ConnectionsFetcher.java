package Backend.service.datafetcher;

import Backend.domain.Connection;
import Backend.repository.ConnectionRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class ConnectionsFetcher implements DataFetcher<List<Connection>> {
    @Autowired
    private ConnectionRepository connectionRepository;

    public List<Connection> get(DataFetchingEnvironment dataFetchingEnvironment) {
        return connectionRepository.findAll();
    }
}
