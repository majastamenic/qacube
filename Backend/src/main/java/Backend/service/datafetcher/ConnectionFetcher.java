package Backend.service.datafetcher;

import Backend.domain.Connection;
import Backend.repository.ConnectionRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConnectionFetcher implements DataFetcher<Connection> {
    @Autowired
    private ConnectionRepository connectionRepository;

    public Connection get(DataFetchingEnvironment dataFetchingEnvironment) {
        String name = dataFetchingEnvironment.getArgument("name");
        return connectionRepository.findConnectionByName(name);
    }
}
