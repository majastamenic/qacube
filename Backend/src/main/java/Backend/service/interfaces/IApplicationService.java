package Backend.service.interfaces;

import Backend.domain.Application;
import Backend.domain.Connection;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IApplicationService {

    List<Application> getAllApplications();

    Application editApplication(Application application);

    void delete(Long id);

    Application createApplication(Application application);

    void deleteConnection(Connection connection);
}
