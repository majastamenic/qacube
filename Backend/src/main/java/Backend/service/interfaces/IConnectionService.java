package Backend.service.interfaces;

import Backend.domain.Connection;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IConnectionService {

    List<Connection> getAllConnections();

    Connection editConnection(Connection newConnection);

    void deleteConnection(Long id);

    Connection createConnection(Connection connection) throws Exception;

    Connection findConnectionById(Long id);
}
