package Backend.service.interfaces;

import Backend.domain.Commit;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ICommitService {

    List<Commit> getCommits(Long connectionId);

}
