package Backend.service.interfaces;

import Backend.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IUserService {
    User login(User user);
    User loginGoogle(String email);
    User findByEmail(String email);
    List<User> findAll();
}
