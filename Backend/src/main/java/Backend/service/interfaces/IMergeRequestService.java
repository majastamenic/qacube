package Backend.service.interfaces;

import Backend.domain.MergeRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IMergeRequestService {

    List<MergeRequest> getMergeRequests(Long connectionId) throws Exception;

}
