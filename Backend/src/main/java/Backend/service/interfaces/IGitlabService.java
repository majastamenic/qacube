package Backend.service.interfaces;

import Backend.domain.Commit;
import Backend.domain.Connection;
import Backend.domain.MergeRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IGitlabService {

    Connection getProjectID(Connection connection) throws Exception;

    List<MergeRequest> getMergeRequests(Connection connection) throws Exception;

    List<Commit> getCommits(Connection connection);
}
