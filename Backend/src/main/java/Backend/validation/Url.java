package Backend.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UrlValidation.class)
public @interface Url {
    String message() default "Url is not correct.";
    Class<?>[] groups() default {};
    public abstract Class<? extends Payload>[] payload() default {};
}
