import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AddApplicationComponent } from './component/nav-bar/application/add-application/add-application.component';
import { ApplicationComponent } from './component/nav-bar/application/application.component';
import { CommitComponent } from './component/nav-bar/commit/commit.component';
import { HomeComponent } from './component/nav-bar/home/home.component';
import { MergeRequestComponent } from './component/nav-bar/merge-request/merge-request.component';
import { LoginComponent } from './component/login/login.component';
import { ConnectionComponent } from './component/nav-bar/connection/connection.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'navbar', loadChildren: () => import('./component/nav-bar/navbar.module').then(mod => mod.NavbarModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
