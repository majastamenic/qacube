export class MessageDto {
  id: number;
  senderName: string;
  receiverName: string;
  messageContent: string;

  constructor() {
    this.id = 0;
    this.senderName = '';
    this.receiverName = '';
    this.messageContent = '';
  }

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
