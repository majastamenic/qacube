import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BACKEND_PATH, CHAT_PATH, USER_PATH } from 'src/app/util/paths';
import { MessageDto } from './chat.model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private httpClient: HttpClient) { }

  getAll(): any {
    return this.httpClient.get(USER_PATH);
  }

  getMyMessages(fromLogin: any): any {
    return this.httpClient.get(CHAT_PATH + '?receiverName=' + fromLogin);
  }

  sendMessage(message: MessageDto): any{
    return this.httpClient.post(BACKEND_PATH + '/chat', message);
  }
}
