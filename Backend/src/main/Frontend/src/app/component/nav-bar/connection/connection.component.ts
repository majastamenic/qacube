import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Connection, ConnectionDto } from 'src/app/component/nav-bar/connection/connection.models';
import { LoginService } from '../../login/login.service';
import { ConnectionService } from '../../service/connection.service';
import { GraphqlService } from '../../service/graphql.service';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.css']
})
export class ConnectionComponent implements OnInit {

  connections: any;

  enableEdit = false;
  enableEditIndex = null;
  enableShowIndex = null;

  prevConnection: any;

  addConnection = false;
  newConnection = new ConnectionDto();
  query = '';
  enableShowMore = false;

  constructor(private connectionService: ConnectionService, private toastr: ToastrService,
              private router: Router, private loginService: LoginService, private graphQL: GraphqlService) { }

  ngOnInit(): void {

    if (!this.loginService.isUserLogin()) {
      this.router.navigate(['login']);
    }
    this.query = '{ connections { id name } }';
    this.graphQL.getAllGraphQL(this.query).subscribe((connectionList: any) => {
      this.connections = connectionList.data.connections;
    }, (err: any) => {
      this.toastr.info('There is no connection. ' + err.error.message);
    });
  }

  enableEditMethod(e: Event, i: any): void {
    this.prevConnection = this.connections[i];
    this.enableEdit = true;
    this.enableEditIndex = i;
  }

  save(i: any): void {
    this.enableEdit = false;
    this.connectionService.edit(this.connections[i]).subscribe((response: any) => {
      this.connections[i] = new Connection().deserialize(response);
      this.enableEditIndex = null;
      this.toastr.success('Changed ' + response.name + ' connection.');
    }, (err: any) => {
      this.toastr.error('Changing ' + this.connections[i].name + ' connection failed. ' + err.error.message);
    });
  }

  cancel(i: any): void {
    this.enableEditIndex = null;
    this.connections[i] = this.prevConnection;
    this.enableEdit = false;
  }

  delete(i: any): void {
    this.connectionService.delete(this.connections[i].id).subscribe((response: any) => {
      this.toastr.success('Deleted connection.');
      window.location.reload();
    }, (err: any) => {
      this.toastr.error('Deleting connection is not possible. ' + err.error.message);
    });
  }

  create(): void {
    this.connectionService.create(this.newConnection).subscribe((response: any) => {
      this.addConnection = false;
      window.location.reload();
      this.toastr.success('Connection created.');
    }, (err: any) => {
      this.toastr.error('Creating ' + this.newConnection.name + ' connection failed. ' + err.error.message);
    });
  }

  enableCreate(): void {
    this.addConnection = true;
  }

  cancelCreate(): void {
    this.addConnection = false;
  }
  showMore(i: any): void {
    this.enableShowMore = true;
    this.enableShowIndex = i;

    this.query = '{ connection(name: "' + this.connections[i].name + '") {id name url token project branchName} }';
    this.graphQL.getAllGraphQL(this.query).subscribe((response: any) => {
      this.connections[i] = response.data.connection;
    });
  }

  showLess(): void {
    this.enableEdit = false;
    this.enableEditIndex = null;
    this.addConnection = false;
    this.enableShowMore = false;
  }
}
