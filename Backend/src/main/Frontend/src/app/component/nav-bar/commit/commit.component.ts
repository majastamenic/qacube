import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/component/service/application.service';
import { CommitService } from 'src/app/component/nav-bar/commit/commit.service';
import { LoginService } from 'src/app/component/login/login.service';

@Component({
  selector: 'app-commit',
  templateUrl: './commit.component.html',
})
export class CommitComponent implements OnInit {

  applications = [];
  connections = [];

  application: any;
  connection: any;

  stepOne = true;
  stepTwo = false;
  stepCommits = false;

  commits: any = [];

  constructor(private toastr: ToastrService, private applicationService: ApplicationService,
              private commitService: CommitService, private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    if (!this.loginService.isUserLogin()) {
      this.router.navigate(['login']);
    }

    this.applicationService.getAll().subscribe((response: any) => {
      this.applications = response;
    });
  }

  showOne(): void {
    this.connection = '';
    this.stepOne = true;
    this.stepTwo = false;
    this.stepCommits = false;
  }

  showTwo(): void {

    this.stepOne = false;
    this.stepTwo = true;
    this.stepCommits = false;

    this.connections = this.application.connections;
  }

  showCommits(): void {
    this.stepOne = false;
    this.stepTwo = false;
    this.stepCommits = true;

    this.commitService.getAll(this.connection.id).subscribe((response: any) => {
      this.commits = response;
    },
      (err: any) => {
        this.toastr.error('Can not get any commit for connection ' + this.connection.name + '. ' + err.error.message);
      });

  }

  cancel(): void{
    this.application = '';
    this.connection = '';

    this.stepOne = true;
    this.stepTwo = false;
    this.stepCommits = false;
  }

}
