import { Component, OnInit } from '@angular/core';
import { WebSocketService } from '../../service/web-socket.service';
import { NgForm } from '@angular/forms';
import { MessageDto } from './chat.model';
import { ChatService } from './chat.service';
import { User } from '../../login/login.models';
import { LoginService } from '../../login/login.service';
import { Router } from '@angular/router';


import { StompState } from '@stomp/ng2-stompjs';
import { ToastrService } from 'ngx-toastr';

const WEBSOCKET_URL = 'ws://localhost:8080/socket';
const EXAMPLE_URL = '/topic/server-broadcaster';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  users: User[];
  selectedUser = new User();
  messageDto = new MessageDto();
  message = '';
  fromLogin: any;

  messages: MessageDto[];

  constructor(private chatService: ChatService,
              private loginService: LoginService, private router: Router, private toastr: ToastrService) {
    this.users = [];
    this.messages = [];
  }

  ngOnInit(): void {
    if (!this.loginService.isUserLogin()) {
      this.router.navigate(['login']);
    } else {
      this.fromLogin = sessionStorage.getItem('user');
    }
    this.chatService.getAll().subscribe((response: any) => {
      for (const u of response) {
        this.users.push(new User().deserialize(u));
      }
    });

    this.loadMessage();
  }

  sendMessage(): void {
   this.messageDto.senderName = this.fromLogin;
   this.messageDto.receiverName = this.selectedUser.email;

   this.chatService.sendMessage(this.messageDto).subscribe((response: any) => {
     this.messageDto = new MessageDto();
   });

  }

  selectUser(user: any): void {
    this.selectedUser = new User().deserialize(user);
    this.chatService.getMyMessages(this.fromLogin).subscribe((response: any) => {
      this.messages = response;
    });
  }

  loadMessage(): void {
    this.chatService.getMyMessages(this.fromLogin).subscribe((response: any) => {
      for (const message of response) {
        this.messages.push(new MessageDto().deserialize(message));
      }
    });
  }
}
