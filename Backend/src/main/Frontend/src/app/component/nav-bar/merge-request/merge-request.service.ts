import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MERGE_PATH } from '../../../util/paths';

@Injectable({
  providedIn: 'root'
})
export class MergeRequestService {

  constructor(private httpClient: HttpClient) { }

  getAll(connectionId: any): any {
    return this.httpClient.get(MERGE_PATH + '/' + connectionId);
  }
}
