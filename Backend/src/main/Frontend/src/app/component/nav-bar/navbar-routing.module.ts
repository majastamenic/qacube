import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApplicationComponent } from './application/application.component';
import { ChatComponent } from './chat/chat.component';
import { CommitComponent } from './commit/commit.component';
import { ConnectionComponent } from './connection/connection.component';
import { HomeComponent } from './home/home.component';
import { MergeRequestComponent } from './merge-request/merge-request.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'connection', component: ConnectionComponent },
  { path: 'application', loadChildren: () => import('./application/application.module').then(mod => mod.ApplicationModule) },
  { path: 'merge-request', component: MergeRequestComponent },
  { path: 'commit', component: CommitComponent },
  { path: 'chat', component: ChatComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NavbarRoutingModule { }
