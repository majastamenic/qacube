import { Deserializable } from 'src/app/util/deserializable.model';

export class ConnectionDto implements Deserializable {
    name: string;
    url: string;
    token: string;
    project: string;
    branchName: string;

    constructor(){
        this.name = '';
        this.url = '';
        this.token = '';
        this.project = '';
        this.branchName = '';
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }

}

export class Connection implements Deserializable {
    id: number;
    name: string;
    url: string;
    token: string;
    project: string;
    branchName: string;

    constructor(){
        this.id = 0;
        this.name = '';
        this.url = '';
        this.token = '';
        this.project = '';
        this.branchName = '';
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }


}
