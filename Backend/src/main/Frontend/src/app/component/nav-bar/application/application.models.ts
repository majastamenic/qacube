import { Observable } from 'rxjs';
import { Deserializable } from 'src/app/util/deserializable.model';
import { Connection } from '../connection/connection.models';

export class Application implements Deserializable {
    name: string;
    connections: Connection[];

    constructor() {
        this.name = '';
        this.connections = [];
    }
    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}

