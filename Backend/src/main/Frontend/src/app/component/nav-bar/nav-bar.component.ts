import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../service/connection.service';
import { LoginService } from '../login/login.service';
import { ToastrService } from 'ngx-toastr';
import { WebSocketService } from '../service/web-socket.service';
import { Message } from '@stomp/stompjs';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(public loginService: LoginService, private connections: ConnectionService,
              private toastr: ToastrService) {
   }

  ngOnInit(): void {
  }

}
