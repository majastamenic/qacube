import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Application } from 'src/app/component/nav-bar/application/application.models';
import { LoginService } from 'src/app/component/login/login.service';
import { ApplicationService } from '../../../service/application.service';
import { Connection } from '../../connection/connection.models';
import { ConnectionService } from '../../../service/connection.service';

@Component({
  selector: 'app-add-application',
  templateUrl: './add-application.component.html',
  styleUrls: ['./add-application.component.css']
})
export class AddApplicationComponent implements OnInit {

  newApplication = new Application();

  connections: Connection[];
  selectedItems = [];

  constructor(private toastr: ToastrService, private applicationService: ApplicationService,
              private connectionService: ConnectionService, private router: Router, private loginService: LoginService) {
    this.connections = [];
  }

  ngOnInit(): void {

    if (!this.loginService.isUserLogin()) {
      this.router.navigate(['login']);
    }

    this.connectionService.getAll().subscribe((response: any) => {
      for (const con of response) {
        this.connections.push(new Connection().deserialize(con));
      }
    });
  }

  createApp(): void {
    this.applicationService.create(this.newApplication).subscribe((response: any) => {
      this.toastr.success('Application created.');
      this.router.navigate(['navbar/application']);
    }, (err: any) => {
      this.toastr.error('Creating ' + this.newApplication.name + ' application failed. ' + err.error.message);
    });
  }

  cancelCreateApp(): void {
    this.router.navigate(['navbar/application']);
    this.newApplication = new Application();
  }

}
