import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/component/service/application.service';
import { MergeRequestService } from 'src/app/component/nav-bar/merge-request/merge-request.service';
import { LoginService } from '../../login/login.service';
import { Router } from '@angular/router';
import { Application } from '../application/application.models';
import { Connection } from '../connection/connection.models';

@Component({
  selector: 'app-merge-request',
  templateUrl: './merge-request.component.html',
})
export class MergeRequestComponent implements OnInit {

  applications: Application[];
  connections: Connection[];

  application = new Application();
  connection = new Connection();

  stepOne = true;
  stepTwo = false;
  stepPR = false;

  mergeRequests: any = [];

  constructor(private toastr: ToastrService, private applicationService: ApplicationService,
              private mergeRequestService: MergeRequestService, private loginService: LoginService, private router: Router) {
    this.applications = [];
    this.connections = [];
  }

  ngOnInit(): void {
    if (!this.loginService.isUserLogin()) {
      this.router.navigate(['login']);
    }


    this.applicationService.getAll().subscribe((response: any) => {
      for (const app of response) {
        this.applications.push(new Application().deserialize(app));
      }
      this.applications = response;
    });
  }

  showOne(): void {
    this.stepOne = true;
    this.stepTwo = false;
    this.stepPR = false;
  }

  showTwo(): void {
    this.stepOne = false;
    this.stepTwo = true;
    this.stepPR = false;

    this.connections = this.application.connections;
  }

  showPR(): void {
    this.stepOne = false;
    this.stepTwo = false;
    this.stepPR = true;

    this.mergeRequestService.getAll(this.connection.id).subscribe((response: any) => {
      this.mergeRequests = response;
    }, (err: any) => {
      this.toastr.error('Can not get any merge request for connection ' + this.connection.name + '.' + err.error.message);
    });
  }

  cancel(): void {
    this.application = new Application();
    this.connection = new Connection();

    this.stepOne = true;
    this.stepTwo = false;
    this.stepPR = false;
  }
}
