import { ApplicationModule, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddApplicationComponent } from './add-application/add-application.component';
import { ApplicationComponent } from './application.component';

const routes: Routes = [
  { path: '', component: ApplicationComponent },
  { path: 'add', component: AddApplicationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
