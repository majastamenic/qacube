import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from '@stomp/stompjs';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../../login/login.service';
import { WebSocketService } from '../../service/web-socket.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  constructor(private loginService: LoginService, private router: Router,
              private toastr: ToastrService) {
   }

  ngOnInit(): void {
    if (!this.loginService.isUserLogin()) {
      this.router.navigate(['login']);
    }

  }

}
