import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ApplicationService } from 'src/app/component/service/application.service';
import { ConnectionService } from 'src/app/component/service/connection.service';
import { LoginService } from 'src/app/component/login/login.service';
import { Application } from './application.models';
import { Connection } from '../connection/connection.models';
import { GraphqlService } from '../../service/graphql.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {

  applications: any;

  enableEdit = false;
  enableEditIndex = null;

  prevApplication: any;
  connections: Connection[];

  query = '';
  enableShowMore = false;
  enableShowIndex = null;

  constructor(private toastr: ToastrService, private applicationService: ApplicationService, private connectionService: ConnectionService,
              private router: Router, private loginService: LoginService, private graphQL: GraphqlService) {
    this.connections = [];
  }

  ngOnInit(): void {
    if (!this.loginService.isUserLogin()) {
      this.router.navigate(['login']);
    }
    this.query = '{ applications {id name } }';
    this.graphQL.getAllGraphQL(this.query).subscribe((response: any) => {
      this.applications = response.data.applications;
    }, (err: any) => {
      this.toastr.info('There is no application. ' + err.error.message);
    });

    this.connectionService.getAll().subscribe((response: any) => {
      for (const con of response) {
        this.connections.push(new Connection().deserialize(con));
      }
    });
  }

  enableEditMethodApp(e: Event, i: any): void {
    this.prevApplication = this.applications[i];
    this.enableEdit = true;
    this.enableEditIndex = i;
  }

  saveApp(i: any): void {
    this.enableEdit = false;
    this.applicationService.edit(this.applications[i]).subscribe((response: any) => {
      this.applications[i] = new Application().deserialize(response);
      this.enableEditIndex = null;
      this.toastr.success('Changed ' + response.name + ' application.');
    }, (err: any) => {
      this.toastr.error('Changing ' + this.applications[i].name + ' application failed. ' + err.error.message);
    });
  }

  cancelApp(i: any): void {
    this.enableEditIndex = null;
    this.applications[i] = this.prevApplication;
    this.enableEdit = false;
  }

  deleteApp(i: any): void {
    this.applicationService.delete(this.applications[i].id).subscribe((response: any) => {
      this.toastr.success('Deleted application.');
      window.location.reload();
    }, (err: any) => {
      this.toastr.error('Deleting application is not possible. ' + err.error.message);
    });
  }

  showMore(i: any): void {
    this.enableShowMore = true;
    this.enableShowIndex = i;

    this.query = '{ application(name: "' + this.applications[i].name + '") {id name connections{id name url token project branchName}} }';
    this.graphQL.getAllGraphQL(this.query).subscribe((response: any) => {
      this.applications[i] = response.data.application;
    });
  }

  showLess(): void {
    this.enableEdit = false;
    this.enableEditIndex = null;
    this.enableShowMore = false;
  }

}
