import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { COMMIT_PATH } from '../../../util/paths';

@Injectable({
  providedIn: 'root'
})
export class CommitService {

  constructor(private httpClient: HttpClient) { }

  getAll(connectionId: any): any {
    return this.httpClient.get(COMMIT_PATH + '/' + connectionId);
  }
}
