import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { User, UserDto } from './login.models';
import { LoginService } from './login.service';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';

import { Message } from '@stomp/stompjs';
import { WebSocketService } from '../service/web-socket.service';

const WEBSOCKET_URL = 'ws://localhost:8080/socket';
const EXAMPLE_URL = '/topic/server-broadcaster';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {

  user = new UserDto();

  private webSocketService: WebSocketService;
  messageHistory = [];
  notify: any;



  socialUser: SocialUser = {
    provider: '', id: '', email: '', lastName: '', firstName: '',
    name: '', photoUrl: '', authToken: '', idToken: '', authorizationCode: '', response: ''
  };
  userLogged = {};

  constructor(private loginService: LoginService, private toastr: ToastrService,
              private router: Router, private authService: SocialAuthService) {
                this.webSocketService = new WebSocketService(WEBSOCKET_URL, EXAMPLE_URL);
                this.webSocketService.stream().subscribe((message: Message) => {
                  this.toastr.info(message.body);
                });
              }

  ngOnInit(): void {
    if (this.loginService.isUserLogin()) {
      this.loginService.logOut();
    }
  }

  login(): void {
    this.loginService.login(this.user).subscribe((response: User) => {
      this.user = new UserDto().deserialize(response);
      console.log(response);
      sessionStorage.setItem('user', this.user.email);
      this.router.navigate(['/navbar']);
      this.toastr.success('User logged in successfully.');
    },
      (err: any) => {
        this.toastr.error('Error while login. ' + err.error.message);
      });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(data => {
      this.socialUser = data;
      this.loginGoogle();
    });
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(data => {
      this.socialUser = data;
      this.loginGoogle();
    });
  }

  loginGoogle(): void {
    this.user.email = this.socialUser.email;
    this.loginService.loginGoogle(this.user.email).subscribe((response: any) => {
      this.user = new UserDto().deserialize(response);
      sessionStorage.setItem('user', this.user.email);
      this.router.navigate(['/navbar']);
    });
  }

}
