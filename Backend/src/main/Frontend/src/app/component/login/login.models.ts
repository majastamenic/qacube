import { Deserializable } from 'src/app/util/deserializable.model';

export class UserDto implements Deserializable{
    email: string;
    password: string;

    constructor(){
        this.email = '';
        this.password = '';
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}

export class User implements Deserializable{
    id: number;
    email: string;
    password: string;

    constructor(){
        this.id = 0;
        this.email = '';
        this.password = '';
    }

    deserialize(input: any): this {
        return Object.assign(this, input);
    }
}
