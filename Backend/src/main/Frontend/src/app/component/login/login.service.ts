import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User, UserDto } from './login.models';
import { USER_PATH } from '../../util/paths';
import { SocialAuthService } from 'angularx-social-login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient, private authService: SocialAuthService) { }

  login(user: UserDto): any {
    return this.httpClient.post(USER_PATH, user);
  }

  isUserLogin(): boolean {
    const user = sessionStorage.getItem('user');
    return !(user == null);
  }

  logOut(): void {
    sessionStorage.removeItem('user');
    this.authService.signOut();
  }

  loginGoogle(email: string): any {
    return this.httpClient.get(USER_PATH + '/' + email);
  }
}

