import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { APPLICATION_PATH } from '../../util/paths';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private httpClient: HttpClient) { }

  create(connection: any): any {
    return this.httpClient.post(APPLICATION_PATH, connection);
  }

  getAll(): any {
    return this.httpClient.get(APPLICATION_PATH);
  }

  edit(connection: any): any {
    return this.httpClient.put(APPLICATION_PATH, connection);
  }

  delete(id: any): any {
    return this.httpClient.delete(APPLICATION_PATH + '/' + id);
  }

}
