import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GRAPHQL_PATH } from 'src/app/util/paths';

@Injectable({
  providedIn: 'root'
})
export class GraphqlService {

  constructor(private httpClient: HttpClient) { }

  getAllGraphQL(query: string): any {
    return this.httpClient.post(GRAPHQL_PATH, query);
  }
}
