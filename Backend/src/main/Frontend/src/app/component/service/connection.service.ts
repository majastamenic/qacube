import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConnectionDto } from '../nav-bar/connection/connection.models';
import { CONNECTION_PATH } from '../../util/paths';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  constructor(private httpClient: HttpClient) { }

  create(connection: ConnectionDto): any {
    return this.httpClient.post(CONNECTION_PATH, connection);
  }

  getAll(): any {
    return this.httpClient.get(CONNECTION_PATH);
  }

  edit(connection: any): any {
    return this.httpClient.put(CONNECTION_PATH, connection);
  }

  delete(id: any): any {
    return this.httpClient.delete(CONNECTION_PATH + '/' + id);
  }
}
