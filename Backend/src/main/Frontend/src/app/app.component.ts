import { Component } from '@angular/core';
import { Message } from '@stomp/stompjs';
import { ToastrService } from 'ngx-toastr';
import { WebSocketService } from './component/service/web-socket.service';

const WEBSOCKET_URL = 'ws://localhost:8080/socket';
const EXAMPLE_URL = '/topic/server-broadcaster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Frontend';

  private webSocketService: WebSocketService;

  constructor(private toastr: ToastrService){

    this.webSocketService = new WebSocketService(WEBSOCKET_URL, EXAMPLE_URL);
    this.webSocketService.stream().subscribe((message: Message) => {
      this.toastr.info(message.body);
    });

  }
}
