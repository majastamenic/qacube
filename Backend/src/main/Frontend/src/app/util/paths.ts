export const BACKEND_PATH = 'http://localhost:8080/api';

export const CONNECTION_PATH = BACKEND_PATH + '/connection';
export const APPLICATION_PATH = BACKEND_PATH + '/application';
export const MERGE_PATH = BACKEND_PATH + '/merge-request';
export const COMMIT_PATH = BACKEND_PATH + '/commit';
export const USER_PATH = BACKEND_PATH + '/user';

export const CHAT_PATH = BACKEND_PATH + '/mymessages';

export const GRAPHQL_PATH = BACKEND_PATH + '/graphql';
