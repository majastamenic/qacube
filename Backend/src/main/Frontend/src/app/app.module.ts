import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConnectionComponent } from './component/nav-bar/connection/connection.component';
import { LoginComponent } from './component/login/login.component';
import { AddApplicationComponent } from './component/nav-bar/application/add-application/add-application.component';
import { ApplicationComponent } from './component/nav-bar/application/application.component';
import { CommitComponent } from './component/nav-bar/commit/commit.component';
import { HomeComponent } from './component/nav-bar/home/home.component';
import { MergeRequestComponent } from './component/nav-bar/merge-request/merge-request.component';
import { NavBarComponent } from './component/nav-bar/nav-bar.component';

// social login
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, AmazonLoginProvider } from 'angularx-social-login';
import { ChatComponent } from './component/nav-bar/chat/chat.component';


@NgModule({
  declarations: [
    AppComponent,
    MergeRequestComponent,
    ApplicationComponent,
    ConnectionComponent,
    CommitComponent,
    NavBarComponent,
    HomeComponent,
    AddApplicationComponent,
    LoginComponent,
    ChatComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    NgSelectModule,
    SocialLoginModule,
  ],

  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('857613503157-4kkeerf5qmku0r7edlvmk57f73d92ht1.apps.googleusercontent.com')
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('385064976163664')
          }
        ]
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
